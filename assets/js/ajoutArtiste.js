document.addEventListener('DOMContentLoaded', function(){
    console.log("Initialisation")

    //gestionnaires d'événement
    let ar = document.getElementById('ajoutArtiste')
    //console.log(ar);
    ar.addEventListener("click", ajouterUnArtiste);

    ////Supression -Artistes
    //Recupération de l'id du dernier Artiste du spectacle
    let id = 0;
    id = document.getElementById('idArtiste').value;
    let boutons = document.getElementsByName("suppr")
    boutons.forEach(bouton =>{
        bouton.addEventListener('click',supprimerArtiste);
    });


    function ajouterUnArtiste(event){
        let artisteBox = document.getElementById("Artiste");
        let P = document.createElement("p");

        let labelN = document.createElement("label");
        labelN.setAttribute('class','a-'+id);
        labelN.textContent = " Nom : ";
        P.appendChild(labelN);
        let inputN = document.createElement("input");
        inputN.setAttribute('list','Artistes-nom');
        inputN.setAttribute('name',"nom[]");
        inputN.setAttribute('class','a-'+id);
        P.appendChild(inputN);

        let labelP = document.createElement("label");
        labelP.setAttribute('class','a-'+id);
        labelP.textContent = " Prenom : ";
        P.appendChild(labelP);
        let inputP = document.createElement("input");
        inputP.setAttribute('list','Artistes-prenom');
        inputP.setAttribute('name',"prenom[]");
        inputP.setAttribute('class','a-'+id);
        P.appendChild(inputP);

        let bouton = document.createElement("button");
        bouton.setAttribute('name','suppr');
        bouton.setAttribute('id',id);
        bouton.setAttribute('type','button');
        bouton.textContent = "X";
        bouton.addEventListener('click',supprimerArtiste);
        P.appendChild(bouton);

        artisteBox.appendChild(P);
        id++;
    }

    function supprimerArtiste() {
        let id = this.id;
        let artistes = document.querySelectorAll('.a-'+id);
        artistes.forEach(
            artiste => {
                artiste.remove()
            }
        );
        this.remove();
    }
});