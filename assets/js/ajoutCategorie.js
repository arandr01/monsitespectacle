document.addEventListener('DOMContentLoaded', function(){
    console.log("Initialisation")

    //gestionnaires d'événement
    let modifierInput = document.getElementsByName('modifierCat');
    modifierInput.forEach(bouton =>{
        bouton.addEventListener('click',modifier)
    });

    let boutonAjout = document.getElementById('ajoutCategorie')
    boutonAjout.addEventListener('click',ajouterCategorie);

    function modifier(event){
        let id = "input-"+this.id;
        let li = document.getElementById(id);
        let input = document.createElement("input");
        input.setAttribute('name','idCategories[]');
        input.setAttribute('value',this.classList.value);
        li.textContent = '';
        li.appendChild(input);
    }

    function ajouterCategorie(event){
        let form = document.getElementById('form');
        let submit = document.getElementById('submit');

        let div = document.createElement('div');

        let ul = document.createElement('ul');
        ul.setAttribute('class','list-group')

        let li = document.createElement('li');
        li.setAttribute('class','list-group-item');

        let input = document.createElement("input");
        input.setAttribute('name','idCategories[]');

        let label = document.createElement('label');
        label.setAttribute('for','check');
        label.textContent = 'Selectionner: ';

        let checkBox = document.createElement('input');
        checkBox.setAttribute('type','checkbox');
        checkBox.setAttribute('id','check');
        checkBox.setAttribute('name','supprimer[]');
        checkBox.setAttribute('value',this.name);

        li.appendChild(input);
        ul.appendChild(li);
        ul.appendChild(label);
        ul.appendChild(checkBox);
        div.appendChild(ul);
        form.insertBefore(div,submit);
    }
});