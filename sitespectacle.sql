-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 31 déc. 2021 à 16:27
-- Version du serveur : 5.7.36
-- Version de PHP : 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sitespectacle`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `idPers` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`idPers`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`idPers`, `email`, `nom`) VALUES
(1, 'aaronrandria@gmail.com', 'Randrianarisona');

-- --------------------------------------------------------

--
-- Structure de la table `artiste`
--

DROP TABLE IF EXISTS `artiste`;
CREATE TABLE IF NOT EXISTS `artiste` (
  `idArtiste` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `spectacle` int(11) NOT NULL,
  PRIMARY KEY (`idArtiste`)
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `artiste`
--

INSERT INTO `artiste` (`idArtiste`, `nom`, `prenom`, `spectacle`) VALUES
(1, 'Compagnie Trabucco', '', 4),
(2, 'Ayrau', 'Cyril', 3),
(164, 'Metay', 'Yohann', 2),
(5, 'Cika', 'Pierr', 7),
(6, 'Ayrau', 'Cyril', 6),
(44, 'Barbereau', 'Hervé', 15),
(182, 'Orchestre National de France', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `idCategorie` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`idCategorie`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`idCategorie`, `type`) VALUES
(1, 'Opéra'),
(2, 'One Man Show'),
(3, 'Magie/Mentalisme'),
(4, 'Comédie Musicale');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `idPers` int(11) NOT NULL AUTO_INCREMENT,
  `civilite` varchar(5) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `pays` varchar(30) NOT NULL,
  `codepostal` int(6) NOT NULL,
  `ville` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mdp` varchar(61) NOT NULL,
  `tel` int(11) NOT NULL,
  PRIMARY KEY (`idPers`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`idPers`, `civilite`, `nom`, `prenom`, `pays`, `codepostal`, `ville`, `email`, `mdp`, `tel`) VALUES
(1, 'Mr.', 'Randrianarisona', 'Aaron', 'France', 17000, 'la rochelle', 'aaronrandria@gmail.com', '$2y$10$aJ2D/Xi2u1BdFw0P6iXBp.hXpXEJ7HynPpRGFClcjO0osBEUUkfau', 633412787),
(2, 'Mr.', 'Renaut', 'Didi', 'France', 75000, 'Paris', 'gggggggg@glkl.com', '$2y$10$C/OtPk9oFeEWBXuNdOu/8.TYRyKhDiKFZ.Xl4oVeScyIcJY9LxPym', 5555555),
(62, 'Mr.', 'Mistererere', 'CCC', 'France', 88888, 'Paris', 'essai@gmail.ccom', '$2y$10$opVC970tA.ZQkv3XJVjXtOkYYhbcy0fB3Z4xVIE1k8oN9yOTYWqJ.', 20202020),
(63, 'Mr.', 'Mistererere', 'CCC', 'France', 88888, 'Paris', 'essai@gmail.ccosm', '$2y$10$EQnepVu1vzR8M1sOLdhF/.8PqVenEQDXvvLnvKBOsTXhSzvcSKEOy', 20202020),
(64, 'Mr.', 'TEST', 'CLIENT-TEST', 'test', 17000, 'la rochelle', 'client@client.com', '$2y$10$aSoLdi4qE.E3raRjMh7k7OYeMSCJXsdIqpG7YYymMNAG358fNqu4O', 20202020);

-- --------------------------------------------------------

--
-- Structure de la table `ligne_reservation`
--

DROP TABLE IF EXISTS `ligne_reservation`;
CREATE TABLE IF NOT EXISTS `ligne_reservation` (
  `idReservation` int(11) NOT NULL,
  `idSpectacle` int(11) NOT NULL,
  `nbPlace` int(11) NOT NULL,
  PRIMARY KEY (`idReservation`,`idSpectacle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ligne_reservation`
--

INSERT INTO `ligne_reservation` (`idReservation`, `idSpectacle`, `nbPlace`) VALUES
(52, 2, 5),
(52, 4, 1),
(50, 3, 2),
(60, 3, 1),
(59, 3, 1),
(60, 7, 2),
(62, 15, 1),
(62, 7, 2),
(62, 6, 1),
(65, 1, 1),
(65, 15, 2),
(66, 6, 1),
(66, 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `idReservation` int(11) NOT NULL AUTO_INCREMENT,
  `client` int(11) NOT NULL,
  `date` varchar(100) NOT NULL,
  `prixTotal` decimal(11,3) NOT NULL,
  PRIMARY KEY (`idReservation`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`idReservation`, `client`, `date`, `prixTotal`) VALUES
(52, 1, '2021-Dec-Sat 10:12:09', '116.000'),
(50, 1, '2021-Dec-Sat 10:12:09', '40.000'),
(59, 1, '2021-Dec-Wed 09:12:04', '20.000'),
(60, 1, '2021-Dec-Fri 11:12:59', '86.000'),
(62, 1, '2021-Dec-Fri 11:12:07', '112.000'),
(65, 64, '2021-Dec-Fri 04:12:27', '59.000'),
(66, 64, '2021-Dec-Fri 04:12:20', '60.000');

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `idSalle` int(11) NOT NULL AUTO_INCREMENT,
  `nomSalle` varchar(100) NOT NULL,
  `lieu` varchar(100) NOT NULL,
  `nbPlaces` int(11) NOT NULL,
  PRIMARY KEY (`idSalle`),
  UNIQUE KEY `nomSalle` (`nomSalle`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`idSalle`, `nomSalle`, `lieu`, `nbPlaces`) VALUES
(1, 'Théâtre des Champs Elysées', '15, avenue Montaigne 75008 Paris', 2000),
(2, 'L\'espace V.O', '1899 chemin de Paulet 82000 Montauban', 200),
(3, 'Coul\'Théâtre', '7 rue du Général Leclerc 77120 Coulommiers', 80),
(4, 'Le Mail - Scène Culturelle', '7 Rue Jean de Dormans 02200 Soissons', 500),
(5, 'La Chocolaterie', '680 rue Théophraste Renaudot 34430 Saint-Jean-de-Védas', 50),
(6, 'Café Théâtre le Flibustier', '7, rue des Bretons 13100 Aix en Provence', 56),
(13, 'Palais du Rire', '39 Rue du Portail Magnanen 84000 Avignon', 49);

-- --------------------------------------------------------

--
-- Structure de la table `spectacle`
--

DROP TABLE IF EXISTS `spectacle`;
CREATE TABLE IF NOT EXISTS `spectacle` (
  `idSpectacle` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) NOT NULL,
  `thematique` varchar(30) NOT NULL,
  `duree` int(11) NOT NULL,
  `accroche` text NOT NULL,
  `description` text NOT NULL,
  `categorie` int(11) NOT NULL,
  `salle` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `prix` decimal(11,0) NOT NULL,
  PRIMARY KEY (`idSpectacle`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `spectacle`
--

INSERT INTO `spectacle` (`idSpectacle`, `titre`, `thematique`, `duree`, `accroche`, `description`, `categorie`, `salle`, `img`, `prix`) VALUES
(1, 'Eugène Onéguine - Piotr Ilitch Tchaïkovski', 'Opera', 180, 'Opéra en trois actes et sept tableaux (1878)', 'Nouvelle production\r\n\r\nLivret de Constantin Chilovsky et du compositeur;\r\nD\'après Eugène Onéguine d\'Alexandre Pouchkine\r\n\r\nKarina Canellakis | direction\r\nStéphane Braunschweig | mise en scène et scénographie\r\nMarion Lévy | chorégraphie\r\nThibault Vancraenenbroeck | costumes\r\nMarion Hewlett | lumières\r\nMireille Delunsch | Madame Larina\r\nGelena Gaskarova | Tatiana\r\nAlisa Kolosova | Olga\r\nJean-François Borras | Vladimir Lenski\r\nJean-Sébastien Bou | Eugène Onéguine\r\nJean Teitgen | Prince Grémine\r\nDelphine Haidan | Filippievna\r\nYuri Kissin | Le Capitaine / Zaretski\r\nMarcel Beekman | Monsieur Triquet\r\n\r\nOrchestre National de France\r\nChoeur de l\'Opéra National de Bordeaux\r\nDirection Salvatore Caputo\r\n\r\nA Savoir :\r\nOpéra chanté en russe; surtitré en français et en anglais.\r\nPour des raisons de sécurité; l\'accès au théâtre est interdit aux enfants de moins de 3 ans.\r\n\r\nPass sanitaire obligatoire :\r\nVotre séance est soumise à des conditions d\'accès spécifiques :\r\nLe pass sanitaire sera exigé à l\'entrée du site. Vous devrez présenter individuellement :\r\nEn format papier ou en format numérique disponible dans l\'application mobile Tousanticovid ou sur https://attestation-vaccin.ameli.fr/\r\n- La preuve d\'un test négatif (PCR ou antigénique) de moins de 48h OU\r\n- Un certificat de vaccination OU\r\n- Un certificat de rétablissement de la Covid-19 ET\r\n- Une pièce d\'identité\r\nSans présentation de votre pass; l\'accès à la séance vous sera refusé.', 1, 1, 'assets/images/spectacle/eugene.jpeg', '21'),
(2, 'Yohann Métay dans Le sublime Sabotage', 'Humour', 100, 'Retiré dans sa longère angevine; un comédien a renoncé à la course aux Molières et à sa gloire après des années de tournées...', 'Se contentant de vente de fromage et de Yoga; il aspire à la paix. Mais la découverte d\\\\\\\'une vieille critique le concernant va remettre le feu dans ce volcan éteint et le lancer dans une aventure de la création qui l\\\\\\\'entraînera dans une spirale burlesque d\\\\\\\'où il ne ressortira pas flamboyant mais peut-être un peu plus libre qu\\\\\\\'avant.\r\n\r\nAprès \\\\\\\" La Tragédie du Dossard 512 \\\\\\\"; Yohann Métay troque son lycra pour une plume et nous plonge dans le tourbillon de l\\\\\\\'écriture de son nouveau spectacle. La pression sur les épaules et la trouille au bide; une spirale de la lose d\\\\\\\'un type qui voulait penser plus haut que son QI.', 2, 2, 'assets/images/spectacle/LeSublimeSabotage.jpeg', '20'),
(3, 'Magic show', 'Humour', 50, 'Ah si j\'étais magicien !', 'Dans ce spectacle, Cyril va vous raconter une histoire de magicien à hauteur d\'enfants. Depuis l\'âge de 11 ans, il rêve de pouvoir un jour émerveiller petits et grands. Alors, pas à pas, il apprend des tours de magie qui lui ont permis d\'épater ses amis à l\'école, puis sa famille pour aujourd\'hui être là, devant vous, et faire de vous de véritables assistant(e)s magicien(ne)s.\r\n\r\nConçu principalement pour les jeunes enfants de 3 ans à 14 ans, ce spectacle mêlant humour, magie et féérie, vous emportera dans un univers extraordinairement magique.', 3, 3, 'assets/images/spectacle/MagicShow.jpeg', '20'),
(4, 'Si on chantait ?', '', 110, 'De nombreux clins d\'oeil aux magnifiques histoires de Pagnol, revisitées et modernisées par la compagnie Trabucco !', 'Venez découvrir notre spectacle musical qui aura pour fil conducteur l\'amour des mots et le sens du texte...\r\n\r\nDans un charmant petit port du Sud de la France, la vie bat son plein. Les habitants y sont heureux même si la routine a envahit le quotidien. Il n\'y a qu\'à voir comme la femme du boulanger s\'ennuie...\r\nDe son côté, le Maire du village prépare en secret un projet auquel les habitants vont rapidement s\'opposer, puis, l\'arrivée au port d\'un séduisant marin va chambouler leurs existences.\r\nDe nombreux clins d\'oeil aux magnifiques histoires de Pagnol, revisitées et modernisées par la compagnie Trabucco ! Une histoire pleine d\'humour et d\'émotions où le public est invité à revivre des moments inoubliables au rythme des grands noms de la chanson française.\r\n\r\nAu programme, vous retrouverez les chansons les plus connues et incontournables de Jacques Brel, Charles Trenet, Barbara, Edith Piaf, Fernand Sardou, Juliette Greco, Charles Aznavour, Mike Brant, Claude François, Véronique Sanson, Jean Ferrat, Dalida, Hélène Segara, Michel Sardou, France Gall, Joe Dassin, Johnny Hallyday, Michel Fugain, Serge Gainsbourg, Mireille Mathieu, Claude Nougaro, Michelle Torr, Daniel Balavoine, Sylvie Vartan, Eric Morena, Hugues Aufray, Diane Tell, Michel Polnareff, Pierre Bachelet, Gilbert Montagné.\r\n\r\nVenez vibrer avec nos artistes pour 2h de spectacle, dansé et chanté en direct !', 4, 4, 'assets/images/spectacle/SiOnChantait.jpeg', '16'),
(6, 'Cyril Ayrau dans Confidences', 'Humour', 70, 'C\'est magique !', 'Avec son personnage véritablement attachant, Cyril Ayrau, illusionniste, mêle son rôle de magicien célibataire et l\'improvisation de son nouveau rôle de papa à travers une magie théâtralisée.\r\n\r\nDurant plus d\'une heure vous découvrirez un spectacle passionnant où l\'amour et la paternité sont mis à l\'honneur.\r\n\r\nConfidences, c\'est le spectacle interactif à partager en famille dès 6 ans. Les effets de magie comme le texte sont choisis pour que tous les publics y trouvent quelque chose pour rire ou rêver.\r\n', 3, 3, 'https://www.billetreduc.com/f700-600-0/vz-8cd09f58-e53d-4e92-846f-6c3f78dead3f.jpeg', '27'),
(7, 'Pierr Cika dans Hypnose Xpérience', 'Humour', 90, 'Un show à dormir debout où les spectateurs découvrent le pouvoir de leur subconscient à travers ce moment plein de surprises, de rires et d\'émotions', 'Durant près de deux heures vous partagerez en groupe, une expérience hors du commun.\r\nDes plus interactifs, ce spectacle d\'hypnose aborde cette discipline comme rarement elle a pu être abordée.\r\n- Endormissements en rafale, modifications des sens, de la mémoire, du comportement.\r\n- Des inductions hypnotiques liées à des sons d\'ambiance pour plonger les sujets (ainsi que les spectateurs) encore plus loin dans l\'action.', 3, 5, 'https://www.billetreduc.com/f700-700-0/vz-820174b6-cf6a-48f4-b774-8f4fa3e0ce8b.jpeg', '33'),
(15, 'Hervé Barbereau dans L\'hypnose à travers le temps', 'Humour', 74, 'Hervé Barbereau est l\'un des rares hypnotiseurs à savoir hypnotiser en quelques secondes.', 'Après \"Hypnosis\" (plus de 640 représentations en 4 ans), venez découvrir le nouveau spectacle d\'Hervé Barbereau : \"L\'Hypnose à travers le temps\".  Une fois hypnotisé, vous traverserez les siècles, des origines de l\'hypnose à nos jours, sous l\'oeil bienveillant d\'Hervé qui mène d\'une main de maître ce spectacle où l\'humour et les anecdotes s\'entremêlent.', 4, 13, 'assets/images/spectacle/vz-9C1D733A-A59D-49A6-845D-D787CC1ABAED.jpeg', '19');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
