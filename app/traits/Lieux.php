<?php

namespace app\traits;

trait Lieux
{
    private string $nomSalle;
    private string $lieu;
    private int $nbPlaces;

    /**
     * @return string
     */
    public function getNomSalle(): string
    {
        return $this->nomSalle;
    }

    /**
     * @param string $nomSalle
     */
    public function setNomSalle(string $nomSalle): void
    {
        $this->nomSalle = $nomSalle;
    }

    /**
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return int
     */
    public function getNbPlaces(): int
    {
        return $this->nbPlaces;
    }

    /**
     * @param int $nbPlaces
     */
    public function setNbPlaces(int $nbPlaces): void
    {
        $this->nbPlaces = $nbPlaces;
    }

}