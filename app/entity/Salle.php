<?php

namespace app\entity;

class Salle
{
    private int $idSalle;
    private string $nomSalle;
    private string $lieu;
    private int $nbPlaces;

    public function __construct(array $data)
    {
        $this->hydrate($data);
    }
    public function hydrate(array $donnee){
        foreach ($donnee as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return int
     */
    public function getIdSalle(): int
    {
        return $this->idSalle;
    }

    /**
     * @param int $idSalle
     */
    public function setIdSalle(int $idSalle): void
    {
        $this->idSalle = $idSalle;
    }

    /**
     * @return string
     */
    public function getNomSalle(): string
    {
        return $this->nomSalle;
    }

    /**
     * @param string $nomSalle
     */
    public function setNomSalle(string $nomSalle): void
    {
        $this->nomSalle = $nomSalle;
    }

    /**
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return int
     */
    public function getNbPlaces(): int
    {
        return $this->nbPlaces;
    }

    /**
     * @param int $nbPlaces
     */
    public function setNbPlaces(int $nbPlaces): void
    {
        $this->nbPlaces = $nbPlaces;
    }


}