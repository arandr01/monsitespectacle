<?php

namespace app\entity;

class Artiste
{
    private int $idArtiste;
    private string $nom;
    private string $prenom;
    private int $spectacle;

    public function __construct(array $data)
    {
        $this->hydrate($data);
    }
    public function hydrate(array $donnee){
        foreach ($donnee as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }
    /**
     * @return int
     */
    public function getIdArtiste(): int
    {
        return $this->idArtiste;
    }

    /**
     * @param int $idArtiste
     */
    public function setIdArtiste(int $idArtiste): void
    {
        $this->idArtiste = $idArtiste;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return int
     */
    public function getSpectacle(): int
    {
        return $this->spectacle;
    }

    /**
     * @param int $spectacle
     */
    public function setSpectacle(int $spectacle): void
    {
        $this->spectacle = $spectacle;
    }

    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return $this->getNom();
    }


}