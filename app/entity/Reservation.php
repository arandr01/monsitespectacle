<?php

namespace app\entity;

class Reservation
{
    private int $idReservation;
    private int $client;
    private string $date;
    private float $prixTotal;
    private array $lignesReservation;
    private Client $clientOBJ;

    public function __construct(array $data)
    {
        $this->lignesReservation = array();
        $this->hydrate($data);
    }
    public function hydrate(array $donnee){
        foreach ($donnee as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return int
     */
    public function getIdReservation(): int
    {
        return $this->idReservation;
    }

    /**
     * @param int $idReservation
     */
    public function setIdReservation(int $idReservation): void
    {
        $this->idReservation = $idReservation;
    }

    /**
     * @return int
     */
    public function getClient(): int
    {
        return $this->client;
    }

    /**
     * @param int $client
     */
    public function setClient(int $client): void
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getPrixTotal(): float
    {
        return $this->prixTotal;
    }

    /**
     * @param float $prixTotal
     */
    public function setPrixTotal(float $prixTotal): void
    {
        $this->prixTotal = $prixTotal;
    }

    /**
     * @return array
     */
    public function getLignesReservation(): array
    {
        return $this->lignesReservation;
    }

    /**
     * @param array $lignesReservation
     */
    public function setLignesReservation(array $lignesReservation): void
    {
        $this->lignesReservation = $lignesReservation;
    }

    public function addLigneReservation (LigneReservation $ligne) : void
    {
        $this->lignesReservation[] = $ligne;
    }

    /**
     * @return Client
     */
    public function getClientOBJ(): Client
    {
        return $this->clientOBJ;
    }

    /**
     * @param Client $clientOBJ
     */
    public function setClientOBJ(Client $clientOBJ): void
    {
        $this->clientOBJ = $clientOBJ;
    }


    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return $this->getPrixTotal();
    }

}