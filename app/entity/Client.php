<?php

namespace app\entity;

class Client
{
    private int $idPers;
    private string $civilite;
    private string $nom;
    private string $prenom;
    private string $pays;
    private int $codepostal;
    private string $ville;
    private string $email;
    private string $mdp;

    public function __construct(array $data)
    {
        $this->hydrate($data);
    }
    public function hydrate(array $donnee){
        foreach ($donnee as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return int
     */
    public function getIdPers(): int
    {
        return $this->idPers;
    }

    /**
     * @param int $idPers
     */
    public function setIdPers(int $idPers): void
    {
        $this->idPers = $idPers;
    }

    /**
     * @return string
     */
    public function getCivilite(): string
    {
        return $this->civilite;
    }

    /**
     * @param string $civilite
     */
    public function setCivilite(string $civilite): void
    {
        $this->civilite = $civilite;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getPays(): string
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays(string $pays): void
    {
        $this->pays = $pays;
    }

    /**
     * @return int
     */
    public function getCodepostal(): int
    {
        return $this->codepostal;
    }

    /**
     * @param int $codepostal
     */
    public function setCodepostal(int $codepostal): void
    {
        $this->codepostal = $codepostal;
    }

    /**
     * @return string
     */
    public function getVille(): string
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille(string $ville): void
    {
        $this->ville = $ville;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getMdp(): string
    {
        return $this->mdp;
    }

    /**
     * @param string $mdp
     */
    public function setMdp(string $mdp): void
    {
        $this->mdp = $mdp;
    }

    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return $this->getNom();
    }
}