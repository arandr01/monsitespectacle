<?php

namespace app\entity;

class Admin
{
    private int $idPers;
    private string $nom;
    private string $prenom;

    public function __construct(array $data)
    {
        $this->hydrate($data);
    }
    public function hydrate(array $donnee){
        foreach ($donnee as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return int
     */
    public function getIdPers(): int
    {
        return $this->idPers;
    }

    /**
     * @param int $idPers
     */
    public function setIdPers(int $idPers): void
    {
        $this->idPers = $idPers;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

}