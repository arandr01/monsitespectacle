<?php
namespace app\entity;

use app\entity\Categorie;
use app\entity\Salle;
class Spectacle
{
    private int $idSpectacle = 0;
    private string $titre ;
    private string $thematique;
    private float $duree;
    private string $accroche;
    private string $description;
    private int $categorie = 0;
    private int $salle = 0;
    private string $img = '';
    private float $prix;
    private array $artistes;
    private Categorie $categorieOBJ;
    private Salle $salleOBJ;

    public function __construct(array $data)
    {
        $this->artistes = array();
        $this->hydrate($data);
    }
    public function hydrate(array $donnee){
        foreach ($donnee as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }
    /**
     * @return int
     */
    public function getIdSpectacle(): int
    {
        return $this->idSpectacle;
    }

    /**
     * @param int $idSpectacle
     */
    public function setIdSpectacle(int $idSpectacle): void
    {
        $this->idSpectacle = $idSpectacle;
    }

    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre(string $titre): void
    {
        $this->titre = $titre;
    }

    /**
     * @return string
     */
    public function getThematique(): string
    {
        return $this->thematique;
    }

    /**
     * @param string $thematique
     */
    public function setThematique(string $thematique): void
    {
        $this->thematique = $thematique;
    }

    /**
     * @return float
     */
    public function getDuree(): float
    {
        return $this->duree;
    }

    /**
     * @param float $duree
     */
    public function setDuree(float $duree): void
    {
        $this->duree = $duree;
    }

    /**
     * @return string
     */
    public function getAccroche(): string
    {
        return $this->accroche;
    }

    /**
     * @param string $accroche
     */
    public function setAccroche(string $accroche): void
    {
        $this->accroche = $accroche;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getCategorie(): int
    {
        return $this->categorie;
    }

    /**
     * @param int $categorie
     */
    public function setCategorie(int $categorie): void
    {
        $this->categorie = $categorie;
    }

    /**
     * @return int
     */
    public function getSalle(): int
    {
        return $this->salle;
    }

    /**
     * @param int $salle
     */
    public function setSalle(int $salle): void
    {
        $this->salle = $salle;
    }

    /**
     * @return string
     */
    public function getImg(): string
    {
        return $this->img;
    }

    /**
     * @param string $img
     */
    public function setImg(string $img): void
    {
        $this->img = $img;
    }

    /**
     * @return float
     */
    public function getPrix(): float
    {
        return $this->prix;
    }

    /**
     * @param float $prix
     */
    public function setPrix(float $prix): void
    {
        $this->prix = $prix;
    }

    /**
     * @return array
     */
    public function getArtistes(): array
    {
        return $this->artistes;
    }

    /**
     * @param array $artistes
     */
    public function setArtistes(array $artistes): void
    {
        $this->artistes = $artistes;
    }

    public function addArtiste(Artiste $artiste) : void
    {
        $this->artistes[] = $artiste;
    }

    /**
     * @return Categorie
     */
    public function getCategorieOBJ(): Categorie
    {
        return $this->categorieOBJ;
    }

    /**
     * @param Categorie $categorieOBJ
     */
    public function setCategorieOBJ(Categorie $categorieOBJ): void
    {
        $this->categorieOBJ = $categorieOBJ;
    }

    /**
     * @return Salle
     */
    public function getSalleOBJ(): Salle
    {
        return $this->salleOBJ;
    }

    /**
     * @param Salle $salleOBJ
     */
    public function setSalleOBJ(Salle $salleOBJ): void
    {
        $this->salleOBJ = $salleOBJ;
    }

    public function updates(array $data)
    {
        $this->hydrate($data);
    }
    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return $this->getTitre()."{id : ".$this->getIdSpectacle()." ;categorie : ".$this->getCategorie()." ;salle : ".$this->getSalle();
    }

}