<?php

namespace app\entity;

class LigneReservation
{
    private int $idReservation;
    private int $idSpectacle;
    private int $nbPlace;
    private Spectacle $spectacle;

    public function __construct(array $data)
    {
        $this->hydrate($data);
    }
    public function hydrate(array $donnee){
        foreach ($donnee as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return int
     */
    public function getIdReservation(): int
    {
        return $this->idReservation;
    }

    /**
     * @param int $idReservation
     */
    public function setIdReservation(int $idReservation): void
    {
        $this->idReservation = $idReservation;
    }

    /**
     * @return int
     */
    public function getIdSpectacle(): int
    {
        return $this->idSpectacle;
    }

    /**
     * @param int $idSpectacle
     */
    public function setIdSpectacle(int $idSpectacle): void
    {
        $this->idSpectacle = $idSpectacle;
    }

    /**
     * @return int
     */
    public function getNbPlace(): int
    {
        return $this->nbPlace;
    }

    /**
     * @param int $nbPlace
     */
    public function setNbPlace(int $nbPlace): void
    {
        $this->nbPlace = $nbPlace;
    }

    /**
     * @return Spectacle
     */
    public function getSpectacle(): Spectacle
    {
        return $this->spectacle;
    }

    /**
     * @param Spectacle $spectacle
     */
    public function setSpectacle(Spectacle $spectacle): void
    {
        $this->spectacle = $spectacle;
    }

    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return $this->getIdReservation();
    }

}