<?php

namespace app\model;

use app\config\Database;

class Model
{
    protected $connexion;
    protected $table;

    /**
     * @param $connexion
     * @param $table
     */
    public function __construct()
    {
        $db = new Database();
        $this->connexion = $db->getConnection();
    }

    public function find(array $data=null)
    {
        $conditions = '';
        $fields = '*';
        $limit = '';
        $order = '';
        $othertable = '';
        if(!empty($data['conditions'])){
            $conditions = 'where '.$data['conditions'];
        }
        if(!empty($data['fields'])){
            $fields = $data['fields'];
        }
        if(!empty($data['limit'])){
            $limit = 'limit '.$data['limit'];
        }
        if(!empty($data['order'])){
            $order = 'order by '.$data['order'];
        }
        if(!empty($data['othertable'])){
            $othertable = $data['othertable'];
        }
        $req = 'select '.$fields.' from '.$this->table.' '.$othertable.' '.$conditions.' '.$order.' '.$limit;
        //var_dump($req);
        $reqPrep = $this->connexion->prepare($req);
        $reqPrep->execute();
        return $reqPrep->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function read($id)
    {
        $req = 'select * from '.$this->table.' where id = :id';
        $reqPrep = $this->connexion->prepare($req);
        $reqPrep->bindParam(':id',$id);
        $reqPrep->execute();
        return $reqPrep->fetch(\PDO::FETCH_ASSOC);
    }
    public function save(array $data) : bool
    {
        $fields = '';
        if(isset($data['fields'])){
            $fields = ' ('.$data['fields'].')';
        }
        $req = 'insert into '.$this->table.$fields.' values ('.$data['values'].')';
        //var_dump($req);
        $reqPrep = $this->connexion->prepare($req);
        return $reqPrep->execute();
    }
    public function delete(array $data)
    {
        $conditions = '';
        if(!empty($data['conditions'])){
            $conditions = 'where '.$data['conditions'];
        }
        $req = 'delete from '.$this->table.' '.$conditions;
        $reqPrep = $this->connexion->prepare($req);
        return $reqPrep->execute();
    }

    public function update(array $data)
    {
        $req = "update ".$this->table." set";
        $fields = explode(",",$data['fields']);
        $values = explode(",",$data['values']);
        foreach (array_combine($fields,$values) as $field => $value){
            $req .= " ".$field." = ".$value.",";
        }
        $req =substr($req,0,-1);
        $req .= " where ".$data['conditions'];
        $reqPrep = $this->connexion->prepare($req);
        return $reqPrep->execute();
    }
}