<?php

namespace app\model;

use app\entity\LigneReservation;
use app\entity\Spectacle;

class ModelLigneReservation extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'ligne_reservation';
    }
    public function deleteLignesReservation(int $idReservation)
    {
        $data = array(
            'conditions' => 'idReservation = '.$idReservation
        );
        $this->delete($data);
    }

    public function saveLines(array $data)
    {
        $this->save($data);
    }

    public function findAll(int $idReservation)
    {
        $listeLignes = array();
        $data = array(
            'conditions'=>'idReservation = '.$idReservation
        );
        $tabLignes = $this->find($data);
        foreach ($tabLignes as $dataLigne){
            $ligne = new LigneReservation($dataLigne);
            $this->findSpectacle($ligne);
            $listeLignes[] = $ligne;
        }
        return $listeLignes;
    }
    public function findSpectacle(LigneReservation $ligne) : void
    {
        $data = array(
            'othertable'=>'as Li join spectacle S on S.idSpectacle = Li.idSpectacle',
            'conditions'=>'Li.idSpectacle = '.$ligne->getIdSpectacle()
        );
        $dataSpectacle= $this->find($data);
        $spectacle = new Spectacle($dataSpectacle[0]);
        $ligne->setSpectacle($spectacle);
    }
}