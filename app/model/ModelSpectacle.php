<?php

namespace app\model;

use app\entity\Artiste;
use app\entity\Salle;
use app\entity\Spectacle;
use app\entity\Categorie;
class ModelSpectacle extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'spectacle';
    }

    public function findAll() : array
    {
        $listeSpecOBJ = array();
        $tabSpectacle = $this->find();
        foreach ($tabSpectacle as $dataSpectacle) {
            $spectacle = new Spectacle($dataSpectacle);
            $this->findCategorie($spectacle);
            $this->findArtistes($spectacle);
            $this->findSalle($spectacle);
            $listeSpecOBJ[] = $spectacle;
        }
        return $listeSpecOBJ;
    }

    public function find3() : array
    {
        $listeSpecOBJ = array();
        $data = array(
            'limit'=>3
        );
        $tabSpectacle = $this->find($data);
        foreach ($tabSpectacle as $dataSpectacle) {
            $spectacle = new Spectacle($dataSpectacle);
            $this->findCategorie($spectacle);
            $this->findArtistes($spectacle);
            $this->findSalle($spectacle);
            $listeSpecOBJ[] = $spectacle;
        }
        return $listeSpecOBJ;
    }
    public function findOne(int $idSpectacle) : Spectacle
    {
        $data = array(
            "conditions" => "idSpectacle = ".$idSpectacle
        );
        $dataSpectacle = $this->find($data);
        $spectacle = new Spectacle($dataSpectacle[0]);
        $this->findCategorie($spectacle);
        $this->findArtistes($spectacle);
        $this->findSalle($spectacle);
        return $spectacle;
    }

    public function findParCategorie(int $categorie) : array
    {
        $listeSpectacle = array();
        $data = array(
            'conditions' => 'categorie = '.$categorie
        );
        $tabSpectacle = $this->find($data);
        foreach ($tabSpectacle as $dataSpectacle){
            $spectacle = new Spectacle($dataSpectacle);
            $this->findCategorie($spectacle);
            $this->findArtistes($spectacle);
            $this->findSalle($spectacle);
            $listeSpectacle[] = $spectacle;
        }
        return $listeSpectacle;
    }

    public function find3ParCategorie(int $categorie) : array
    {
        $listeSpectacle = array();
        $data = array(
            'limit' => 3,
            'conditions' => 'categorie = '.$categorie
        );
        $tabSpectacle = $this->find($data);
        foreach ($tabSpectacle as $dataSpectacle){
            $spectacle = new Spectacle($dataSpectacle);
            $this->findCategorie($spectacle);
            $this->findArtistes($spectacle);
            $this->findSalle($spectacle);
            $listeSpectacle[] = $spectacle;
        }
        return $listeSpectacle;
    }

    public function findByTitle(string $recherche) : array
    {
        $listeSpectacle = array();
        $data = array(
            'conditions' => "titre like '%".$recherche."%'"
        );
        $tabSpectacle = $this->find($data);
        foreach ($tabSpectacle as $dataSpectacle){
            $spectacle = new Spectacle($dataSpectacle);
            $this->findCategorie($spectacle);
            $this->findArtistes($spectacle);
            $this->findSalle($spectacle);
            $listeSpectacle[] = $spectacle;
        }
        return $listeSpectacle;
    }
    public function findSalle(Spectacle $spectacle) : void
    {
        $this->table = 'salle';
        $data = array(
            'conditions'=>'idSalle = '.$spectacle->getSalle()
        );
        $dataSalle = $this->find($data);
        $salle = new Salle($dataSalle[0]);
        $this->table = 'spectacle';
        $spectacle->setSalleOBJ($salle);
    }

    public function findCategorie(Spectacle $spectacle) : void
    {
        $data = array(
            'othertable'=>'as S join categorie C on C.idCategorie = S.categorie',
            'conditions'=>'S.categorie = '.$spectacle->getCategorie()
        );
        $dataCategorie = $this->find($data);
        $categorie = new Categorie($dataCategorie[0]);
        $spectacle->setCategorieOBJ($categorie);
    }

    public function findArtistes(Spectacle $spectacle) : void
    {
        $this->table = 'artiste';
        $data = array(
            'conditions'=>'spectacle = '.$spectacle->getIdSpectacle()
        );
        $tabArtistes = $this->find($data);
        foreach ($tabArtistes as $dataArtiste){
            $artiste = new Artiste($dataArtiste);
            $spectacle->addArtiste($artiste);
        }
        $this->table = 'spectacle';
    }

    public function saveSpectacle(Spectacle $spectacle) : int
    {
        $data = array(
            'fields' => 'titre,thematique,duree,accroche,description,categorie,salle,img,prix',
            'values' => "'".$spectacle->getTitre()."','".$spectacle->getThematique()."',".$spectacle->getDuree().",'".$spectacle->getAccroche()."','".$spectacle->getDescription()."',".$spectacle->getCategorie().",".$spectacle->getSalle().",'".$spectacle->getImg()."',".$spectacle->getPrix()
        );
        $this->save($data);
        return $this->connexion->lastInsertId();
    }

    public function deleteSpectacle(int $idSpectacle)
    {
        $data = array(
            'conditions' => 'idSpectacle = '.$idSpectacle
        );
        $this->deleteArtistes($idSpectacle);
        return $this->delete($data);
    }

    public function updateSpectacle(Spectacle $spectacle)
    {
        $data = array(
            'conditions'=> 'idSpectacle = '.$spectacle->getIdSpectacle(),
            'fields' => "titre,thematique,duree,accroche,description,categorie,salle,img,prix",
            'values' => "'".$spectacle->getTitre()."','".$spectacle->getThematique()."',".$spectacle->getDuree().",'".$spectacle->getAccroche()."','".$spectacle->getDescription()."',".$spectacle->getCategorie().",".$spectacle->getSalle().",'".$spectacle->getImg()."',".$spectacle->getPrix()
        );
        return $this->update($data);
    }

    public function findRandom(int $limit,array $tabSpectacle) : array
    {
        $conditions = '';
        if (!empty($tabSpectacle)){
            $conditions = 'idSpectacle not in (';
            foreach ($tabSpectacle as $spect){
                $conditions .= $spect->getIdSpectacle().",";
            }
            $conditions =substr($conditions,0,-1);
            $conditions .= ")";
        }
        $listeSpectacle = array();
        $data = array(
            'limit' => $limit,
            'order' => "rand()",
            'conditions' => $conditions
        );
        $tabSpectacle = $this->find($data);
        foreach ($tabSpectacle as $dataSpectacle) {
            $spectacle = new Spectacle($dataSpectacle);
            $this->findCategorie($spectacle);
            $this->findArtistes($spectacle);
            $this->findSalle($spectacle);
            $listeSpectacle[] = $spectacle;
        }
        return $listeSpectacle;
    }

    public function deleteArtistes(int $idSpectacle)
    {
        $this->table = 'artiste';
        $data = array(
            'conditions'=> 'spectacle = '.$idSpectacle
        );
        $this->delete($data);
        $this->table = 'spectacle';
    }
}