<?php

namespace app\model;

use app\entity\Salle;

class ModelSalle extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'salle';
    }

    public function findAll() : array
    {
        $listeSalle = array();
        $tabSalle = $this->find();
        foreach ($tabSalle as $dataSalle) {
            $Salle = new Salle($dataSalle);
            $listeSalle[] = $Salle;
        }
        return $listeSalle;
    }

    public function findByNom(string $nomSalle)
    {
        $data = array(
            'conditions' => "nomSalle = '" . $nomSalle . "'"
        );
        $dataSalle = $this->find($data);
        if(!empty($dataSalle)){
            return new Salle($dataSalle[0]);
        }
        return $dataSalle;
    }

    public function saveSalle(array $dataSalle)
    {
        $data = array(
            'fields' => 'nomSalle,lieu,nbPlaces',
            'values'=> "'".$dataSalle['nomSalle']."','".$dataSalle['lieu']."',".$dataSalle['nbPlaces']
        );
        $this->save($data);
        return $this->connexion->lastInsertId();
    }
}