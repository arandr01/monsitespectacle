<?php

namespace app\model;

use app\entity\Artiste;

class ModelArtiste extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'artiste';
    }

    public function findAll() : array
    {
        $listeArtiste = array();
        $tabArtiste = $this->find();
        foreach ($tabArtiste as $dataArtiste) {
            $artiste = new Artiste($dataArtiste);
            $listeArtiste[] = $artiste;
        }
        return $listeArtiste;
    }

    public function findAllSpectacleArtiste(int $idSpectacle) : array
    {
        $listeArtiste = array();
        $data = array(
            'conditions' => 'spectacle = '.$idSpectacle
        );
        $tabArtiste = $this->find($data);
        if(empty($tabArtiste)){
            return array();
        }
        foreach ($tabArtiste as $dataArtiste){
            $artiste = new Artiste($dataArtiste);
            $listeArtiste[] = $artiste;
        }
        return $listeArtiste;
    }

    public function saveArtistes(array $noms,int $idSpectacle) : bool
    {
        foreach ($noms as $nom => $prenom){
            $data = array(
                'fields' => 'nom,prenom,spectacle',
                'values' => "'".$nom."','".$prenom."',".$idSpectacle
            );
            if(!$this->save($data)){
                return false;
            }
        }
        return true;
    }

    public function deletesArtistes(int $idSpectacle)
    {
        $data = array(
            'conditions'=> 'spectacle = '.$idSpectacle
        );
        return $this->delete($data);
    }

}