<?php

namespace app\model;

use app\entity\Admin;
use app\entity\Client;
class ModelAdmin extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'admin';
    }

    public function saveAdmin(Client $pers) : bool
    {
        $data = array(
            'fields' => 'idPers,email,nom',
            'values' => $pers->getIdPers().",'".$pers->getEmail()."','".$pers->getNom()."'"
        );
        if($this->save($data)){
            return true;
        }
        return false;
    }

    public function findAdmin(string $email)
    {
        $data = array(
            'conditions'=>"email = '".$email."'"
        );
        $dataAdmin = $this->find($data);
        if(!empty($dataAdmin)){
            return new Admin($dataAdmin[0]);
        }
        return $dataAdmin;
    }
}