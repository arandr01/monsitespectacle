<?php

namespace app\model;

use app\entity\Client;
use app\entity\LigneReservation;
use app\entity\Reservation;
use app\entity\Spectacle;

class ModelReservation extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'reservation';
    }

    public function deleteReservation(int $idReservation)
    {
        $data = array(
            'conditions' => 'idReservation = ' . $idReservation
        );
        $this->delete($data);
    }

    public function startCommande(int $total)
    {
        $date = date('Y-M-D h:m:s');
        $data = array(
            'fields' => 'client,date,prixTotal',
            'values' => $_SESSION['account']->getIdPers() . ",'" . $date . "'," . $total
        );
        $this->save($data);
        $idReservation = $this->connexion->lastInsertId();
        $dataReservation = array(
            'conditions' => 'idReservation = '.$idReservation
        );
        $dataR = $this->find($dataReservation);
        if(!empty($dataR)){
            return new Reservation($dataR[0]);
        }
        return $dataR;
    }

    public function findAll() : array
    {
        $listeResOBJ = array();
        $tabReservation= $this->find();
        foreach ($tabReservation as $dataRes) {
            $reservation = new Reservation($dataRes);
            $this->findLignesCommandes($reservation);
            $this->findClient($reservation);
            $listeResOBJ[] = $reservation;
        }
        return $listeResOBJ;
    }
    public function findLignesCommandes(Reservation $reservation) : void
    {
        $data = array(
            'othertable'=>'as Res join ligne_reservation Li on Li.idReservation = Res.idReservation',
            'conditions'=>'Res.idReservation = '.$reservation->getIdReservation()
        );
        $tabLignes= $this->find($data);
        foreach ($tabLignes as $dataLigne){
            $ligne = new LigneReservation($dataLigne);
            $this->findSpectacle($ligne);
            $reservation->addLigneReservation($ligne);
        }
    }

    public function findClient(Reservation $reservation) : void
    {
        $data = array(
            'othertable'=>'as Res join client Cl on Cl.idPers = Res.client',
            'conditions'=>'Res.idReservation = '.$reservation->getIdReservation()
        );
        $dataClient = $this->find($data);
        if (!empty($dataClient)){
            $client = new Client($dataClient[0]);
            $reservation->setClientOBJ($client);
        }
    }

    public function findSpectacle(LigneReservation $ligne) : void
    {
        $data = array(
            'othertable'=>'as Res join ligne_reservation Li on Li.idReservation = Res.idReservation join spectacle S on S.idSpectacle = Li.idSpectacle',
            'conditions'=>'Li.idSpectacle = '.$ligne->getIdSpectacle()
        );
        $dataSpectacle= $this->find($data);
        $spectacle = new Spectacle($dataSpectacle[0]);
        $ligne->setSpectacle($spectacle);
    }

}