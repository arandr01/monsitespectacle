<?php

namespace app\model;

use app\entity\Client;

class ModelClient extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'client';
    }

    public function findEmail(string $email) : bool
    {
        $data = array(
            'conditions'=>"email = '".$email."'"
        );
        $dataClient = $this->find($data);
        if(!empty($dataClient)){
            return true;
        }
        else{
            return false;
        }
    }

    public function createAccount()
    {
        if(isset($_POST['civilite'],$_POST['nom'],$_POST['prenom'],$_POST['pays'],$_POST['codepostal'],$_POST['ville'],$_POST['email'],$_POST['mdp'], $_POST['tel'])){
            if(!empty($_POST['civilite']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['pays']) && !empty($_POST['codepostal']) && !empty($_POST['ville']) && !empty($_POST['email']) && !empty($_POST['mdp']) && !empty($_POST['tel'])){
                $email = addslashes($_POST['email']);
                if (!$this->findEmail($email)) {
                    $civilite = addslashes($_POST['civilite']);
                    $nom = addslashes($_POST['nom']);
                    $prenom = addslashes($_POST['prenom']);
                    $pays = addslashes($_POST['pays']);
                    $codepostal = addslashes($_POST['codepostal']);
                    $ville = addslashes($_POST['ville']);
                    $mdp = addslashes(($_POST['mdp']));
                    $mdp = password_hash($mdp, PASSWORD_BCRYPT);
                    $tel = addslashes($_POST['tel']);
                    $data = array(
                        'fields' => 'civilite,nom,prenom,pays,codepostal,ville,email,mdp,tel',
                        'values' => "'".$civilite."','".$nom ."','". $prenom ."','". $pays ."','". $codepostal . "','" . $ville . "','".$email."','". $mdp ."','". $tel."'"
                    );
                    if ($this->save($data)) {
                        header('Location: index.php?action=login');
                    } else {
                        print("Une Erreur est Survenue : ");
                    }
                }
                else {
                    print ("Cet Email déjà utiliser !");
                }
            }
            else{
                print("Remplissez tous les champs ");
            }
        }
    }
    public function findOne(string $email) : Client
    {
        $data = array(
            'conditions'=>"email = '".$email."'"
        );
        $dataClient = $this->find($data);
        if(!empty($dataClient)){
            return new Client($dataClient[0]);
        }
        return new $dataClient;
    }
    public function login(array $account)
    {
        if (isset($account['email']) && isset($account['mdp'])){
            $email = addslashes($account['email']);
            $mdp = addslashes($account['mdp']);
            if($this->findEmail($email)){
                $client = $this->findOne($email);
                if(password_verify($mdp,$client->getMdp())){
                    $_SESSION['account'] = $client;
                    header('Location: index.php');
                }
                else{
                    print("le mot de passe ne correspond pas");
                }
            }
            else{
                print("Email non trouvée");
            }
        }
        else {
            print("Remplissez tous les champs");
        }
    }
}