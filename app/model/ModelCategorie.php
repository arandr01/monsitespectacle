<?php

namespace app\model;

use app\entity\Categorie;
use app\entity\Spectacle;

class ModelCategorie extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'categorie';
    }


    public function findOne(int $idCategorie) : Categorie
    {
        $data = array(
            'conditions'=> 'idCategorie = '.$idCategorie
        );
        $dataCategorie = $this->find($data);
        if(!empty($dataCategorie)){
            return new Categorie($dataCategorie[0]);
        }
        return new Categorie(array());
    }
    public function findAll() : array
    {
        $listeCategorie = array();
        $tabCategorie = $this->find();
        foreach ($tabCategorie as $dataCategorie) {
            $Categorie = new Categorie($dataCategorie);
            $listeCategorie[] = $Categorie;
        }
        return $listeCategorie;
    }

    public function findByType(string $type)
    {
        $data = array(
            'conditions'=>"type = '".$type."'"
        );
        $dataCategorie = $this->find($data);
        if(!empty($dataCategorie)){
            return new Categorie($dataCategorie[0]);
        }
        return $dataCategorie;
    }

    public function saveCategorie(string $type) : int
    {
        $data = array(
            'fields'=> 'type',
            'values'=> "'".$type."'"
        );
        $this->save($data);
        return $this->connexion->lastInsertId();
    }

    public function deleteCategorie(int $idCategorie) : bool
    {
        $data = array(
            'conditions' => 'idCategorie = '.$idCategorie
        );
        $this->deleteCatSpectacles($idCategorie);
        return $this->delete($data);
    }

    public function updateCategorie(Categorie $categorie)
    {
        $data = array(
            'fields' => 'type',
            'values' => str_replace(',',';',$categorie->getType()),
            'conditions' => 'idCategorie = '.$categorie->getIdCategorie()
        );
        return $this->update($data);
    }

    public function deleteCatSpectacles(int $categorie)
    {
        $tabSpectacle = $this->findSpectParCategorie($categorie);
        foreach ($tabSpectacle as $spectacle){
            $this->deleteArtistes($spectacle->getIdSpectacle());
        }
        $this->table = 'spectacle';
        $data = array(
            'conditions'=> 'categorie = '.$categorie
        );
        $this->delete($data);
        $this->table = 'categorie';
    }

    public function deleteArtistes(int $idSpectacle)
    {
        $this->table = 'artiste';
        $data = array(
            'conditions'=> 'spectacle = '.$idSpectacle
        );
        $this->delete($data);
        $this->table = 'categorie';
    }

    public function findSpectParCategorie(int $categorie) : array
    {
        $listeSpectacle = array();
        $data = array(
            'othertable' => 'as Cat join spectacle S on Cat.idCategorie = S.categorie',
            'conditions' => 'S.categorie = '.$categorie
        );
        $tabSpectacle = $this->find($data);
        foreach ($tabSpectacle as $dataSpectacle){
            $spectacle = new Spectacle($dataSpectacle);
            $listeSpectacle[] = $spectacle;
        }
        return $listeSpectacle;
    }
}