<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script src="assets/js/ajoutArtiste.js"></script>
    <title>Document</title>
</head>
<body>
<?php
$controllerHeader->getHeader();
?>
<main>
    <div class="container">
        <h1> Administration du site</h1>
        <section>
            <header>
                <nav>
                    <ul>
                        <li><a href="index.php?mode=admin"><h2>Ajout de Spectacle</h2></a></li>
                        <li><a href="index.php?mode=admin&spect=all"><h2>Spectacles</h2></a></li>
                        <li><a href="index.php?mode=admin&transactions=all"><h2>Transactions</h2></a></li>
                        <li><a href="index.php?mode=admin&categorie=all"><h2>Categories</h2></a></li>
                    </ul>
                </nav>
            </header>
            <div style="background-color: #ebebec">
                <form action="index.php?mode=admin" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <legend><h2>Ajout de spectacle</h2></legend>
                        <p>
                            <label for="Titre">Titre : </label>
                            <input type="text" id="Titre" name="titre" required>
                        </p>
                        <p>
                            <label for="Thematique">Thematique : </label>
                            <input type="text" id="Thematique" name="thematique" required>
                        </p>
                        <p>
                            <label for="Duree"> Durée : </label>
                            <input type="number" id="Duree" name="duree" required>
                        </p>
                        <p>
                            <label for="Accroche" style="vertical-align: top"> Accroche : </label>
                            <textarea name="accroche" id="Accroche" cols="50" rows="10"></textarea>
                        </p>
                        <p>
                            <label for="Description" style="vertical-align: top"> Description : </label>
                            <textarea name="description" id="Description" cols="50" rows="15"></textarea>
                        </p>
                        <p>
                            <label for="Prix"> Prix-Place : </label>
                            <input type="number" id="Prix" name="prix" required>
                        </p>
                        <p>
                            <label for="SalleNom"> Nom de Salle : </label>
                            <input list="SallesNom" id="SalleNom" name="nomSalle" style="width: 600px" required> <br>
                            <label for="SalleLieu"> Lieu : </label>
                            <input list="SallesLieu" id="SalleLieu" name="lieu" style="width: 600px" required>
                            <label for="SalleNbPlaces"> NbPlaces : </label>
                            <input type="number" id="SalleNbPlaces" name="nbPlaces" required>
                            <datalist id="SallesNom">
                                <?php foreach ($tabSalle as $salle) : ?>
                                    <option value="<?=$salle->getNomSalle()?>"><?=$salle->getNomSalle()?></option>
                                <?php endforeach;?>
                            </datalist>
                            <datalist id="SallesLieu">
                                <?php foreach ($tabSalle as $salle) : ?>
                                    <option value="<?=$salle->getLieu()?>"><?=$salle->getLieu()?></option>
                                <?php endforeach;?>
                            </datalist>
                        </p>
                        <p>
                            <label for="Categorie">Catégorie : </label>
                            <input list="Categories" id="Categorie" name="categorie" required>
                            <datalist id="Categories">
                                <?php foreach ($tabCategorie as $categorie) : ?>
                                    <option value="<?=$categorie->getType()?>"><?=$categorie->getType()?></option>
                                <?php endforeach;?>
                            </datalist>
                        </p>
                        <p id="Artiste">
                            <label for="Artiste">Artistes : </label> <br>
                            <label for="Artistes-nom"> Nom : </label>
                            <input list="Artistes-nom" id="Artiste" name="nom[]" required>
                            <label for="Artistes-prenom"> Prenom : </label>
                            <input list="Artistes-prenom" id="Artiste" name="prenom[]">
                            <datalist id="Artistes-nom">
                                <?php foreach ($tabArtistes as $artiste) : ?>
                                    <option value="<?=$artiste->getNom()?>"><?=$artiste->getNom()?></option>
                                <?php endforeach;?>
                            </datalist>
                            <datalist id="Artistes-prenom">
                                <?php foreach ($tabArtistes as $artiste) : ?>
                                    <option value="<?=$artiste->getPrenom()?>"><?=$artiste->getPrenom()?></option>
                                <?php endforeach;?>
                            </datalist>
                            <br>
                        </p>
                        <p>
                            <button type="button" id="ajoutArtiste">Ajouter un artiste</button>
                        </p>
                        <p>
                            <label for="image">Image : </label>
                            <input type="file" id="image" name="image" accept="image/jpg" required>
                        </p>
                    </fieldset>
                    <div style="padding: 20px">
                        <input type="submit" id="btnSubmit" name="send" value="Envoyer">
                    </div>
                </form>
            </div>

        </section>
    </div>
<main/>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>