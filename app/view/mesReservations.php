<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<!-- DEBUT de la page -->
<?php
$controllerHeader->getHeader();
?>
<header>
    <h2>Mon Panier</h2>
</header>
<section>
    <?php if (!empty($_SESSION['reservations'])) : ?>
        <table class="table table-hover">
            <ul id="product-list">
                <?php foreach ($_SESSION['reservations'] as $ligne) : ?>
                        <tr style="background-color: #ebebec">
                            <td class="illustration">
                                <img src="<?=$ligne['spect']->getImg()?>" style="height: 600px;width: 424px" alt="">
                            </td>
                            <td class="description" style="width: 400px">
                                <h2><?=$ligne['spect']->getTitre()?></h2>
                                <h3><?=$ligne['spect']->getAccroche()?></h3>
                                <p><?=$ligne['spect']->getDescription()?></p>

                            </td>
                            <td style="width: 300px">
                                <p> <b>Thématique :</b> <?=$ligne['spect']->getThematique()?></p>
                                <p> <b>Durée :</b> <?=$ligne['spect']->getDuree()?> min</p>
                                <p> <b>Catégorie :</b> <?=$ligne['spect']->getCategorieOBJ()->getType()?></p>
                                <p>
                                    <b>Lieu :</b> <?=$ligne['spect']->getSalleOBJ()->getNomSalle()?> <br>
                                    <?=$ligne['spect']->getSalleOBJ()->getLieu()?> <br>
                                    ( <?=$ligne['spect']->getSalleOBJ()->getNbPlaces()?> places)
                                </p>
                                <?php foreach ($ligne['spect']->getArtistes() as $artiste ) : ?>
                                    <p> <b>Artistes : </b> <?=$artiste->getPrenom()?> <?=$artiste->getNom()?></p> <br>
                                <?php endforeach; ?>
                            </td>
                            <td>
                                <h3>Places : </h3>
                                <table class="table-prix">
                                    <thead>
                                        <tr>
                                            <td> Billets </td>
                                            <td> Prix </td>
                                            <td> Total </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <?=$_SESSION['reservations'][$ligne['spect']->getIdSpectacle()]['billets']?>
                                            </td>
                                            <td>
                                                x<?=$ligne['spect']->getPrix()?>€
                                            </td>
                                            <td>
                                                <?=$_SESSION['reservations'][$ligne['spect']->getIdSpectacle()]['billets'] * $ligne['spect']->getPrix()?> €
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?=$_SESSION['reservations'][$ligne['spect']->getIdSpectacle()]['billets'] * $ligne['spect']->getPrix()?> € </td>
                                            <?php $total += $_SESSION['reservations'][$ligne['spect']->getIdSpectacle()]['billets'] * $ligne['spect']->getPrix()?>
                                        </tr>
                                    </tbody>
                                </table>
                                <form action="index.php?action=reserver" method="post">
                                    <input type="hidden" value="<?=$ligne['spect']->getIdSpectacle()?>" name="supprime">
                                    <input type="submit" value="X" name="send">
                                </form>
                            </td>
                        </tr>
                <?php endforeach; ?>
            </ul>
        </table>
        <form action="index.php?action=commande" method="post">
        <button type="submit" style="background-color: burlywood">
            <h3>Total reservations : <?=$total?> € </h3>
            <div>
                <input type="hidden" value="<?=$total?>" name="prixTot">
                <input type="submit" name="send" value="Enregistrer panier">
            </div>
        </button>
        </form>
    <?php else: ?>
        <img src="images/poubelle.png" alt="">
        <h2>Votre panier est vide</h2>
    <?php endif; ?>
</section>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>