<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<?php
$controllerHeader->getHeader();
?>
<main>
    <h1> Administration du site</h1>
    <section class="container">
        <header>
            <nav>
                <ul>
                    <li><a href="index.php?mode=admin"><h2>Ajout de Spectacle</h2></a></li>
                    <li><a href="index.php?mode=admin&spect=all"><h2>Spectacles</h2></a></li>
                    <li><a href="index.php?mode=admin&transactions=all"><h2>Transactions</h2></a></li>
                    <li><a href="index.php?mode=admin&categorie=all"><h2>Categories</h2></a></li>
                </ul>
            </nav>
        </header>
        <form action="index.php?mode=admin&spect=all" method="post">
            <table class="table table-hover">
                <?php foreach ($tabSpectaclesOBJ as $spectacle) : ?>
                    <tr style="background-color: #ebebec">
                        <td>
                            <img src="<?=$spectacle->getImg()?>" style="height: 300px;width: 300px"  alt="">
                        </td>
                        <td>
                            <h2><?=$spectacle->getTitre()?></h2>
                            <h3><?=$spectacle->getAccroche()?></h3>
                            <p><?=tronquer_texte($spectacle->getDescription())?></p>
                            <a href="index.php?mode=admin&spect=<?=$spectacle->getIdSpectacle()?>" class="list-group-item">Modifier</a>
                            <label for="check">Selectionner: </label>
                            <input type="checkbox" id="check" name="supprimer[]" value="<?=$spectacle->getIdSpectacle()?>">
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <input type="submit" name="send" value="supprimer">
        </form>
    </section>
</main>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>
