<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="assets/js/ajoutCategorie.js"></script>
    <title>Document</title>
</head>
<body>
<!-- DEBUT de la page -->
<?php
$controllerHeader->getHeader();
?>
<header>
    <nav>
        <ul>
            <li><a href="index.php?mode=admin"><h2>Ajout de Spectacle</h2></a></li>
            <li><a href="index.php?mode=admin&spect=all"><h2>Spectacles</h2></a></li>
            <li><a href="index.php?mode=admin&transactions=all"><h2>Transactions</h2></a></li>
            <li><a href="index.php?mode=admin&categorie=all"><h2>Categories</h2></a></li>
        </ul>
    </nav>
</header>
<section>
    <div style="background-color: #cccccc" class="container">
        <h3>Categories :</h3>
        <form id="form" action="index.php?mode=admin&categorie=all" method="post">
            <?php foreach ($tabCategorie as $categorie) : ?>
                <div>
                    <ul class="list-group" style="">
                        <li id="input-<?=$categorie->getIdCategorie()?>" class="list-group-item"><?=$categorie->getType()?></li>
                        <li class="list-group-item"><button type="button" id="<?=$categorie->getIdCategorie()?>" name="modifierCat" class="<?=$categorie->getType()?>">Modifier</button> </li>
                        <label for="check">Selectionner: </label>
                        <input type="checkbox" id="check" name="supprimer[]" value="<?=$categorie->getIdCategorie()?>">
                        <ul/>
                </div>
            <?php endforeach; ?>
            <input id="submit" type="submit" name="delete" value="supprimer">
            <button name="<?=$lastIdCat?>" type="button" id="ajoutCategorie">Ajouter Categorie</button>
            <input type="submit" name="send" value="Mettre à Jour">
        </form>
    </div>
</section>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>