<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<?php
$controllerHeader->getHeader();
?>
<main>
    <section class="container">
        <table class="table table-hover" style="display: flex">
            <?php foreach ($tabSpectaclesOBJ as $spectacle) : ?>
                <tr style="background-color: #ebebec">
                    <td>
                        <img src="<?=$spectacle->getImg()?>" style="height: 260px;width: 200px" alt="">
                    </td>
                    <td>
                        <h2><?=$spectacle->getTitre()?></h2>
                        <h3><?=$spectacle->getAccroche()?></h3>
                        <p><?=tronquer_texte($spectacle->getDescription())?></p>
                        <div style="width: 200px">
                            <a href="index.php?spect=<?=$spectacle->getIdSpectacle()?>" class="list-group-item">Voir details</a>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </section>
</main>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>
