<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<?php
$controllerHeader->getHeader();
?>
<main>
    <section class="container">
        <div class="container-fluid">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                    <div class="item active" style="height: 50%">
                        <a href="index.php?spect=<?=$tabSpectaclesOBJ[0]->getIdSpectacle()?>">
                            <img src="<?=$tabSpectaclesOBJ[0]->getImg()?>" style="height: 600px;width: 424px;mi" class="C-slide" alt="">
                            <div class="carousel-caption ">
                                <h3><?=$tabSpectaclesOBJ[0]->getTitre()?></h3>
                                <p><?=$tabSpectaclesOBJ[0]->getAccroche()?></p>
                            </div>
                        </a>
                    </div>

                    <div class="item" style="height: 50%">
                        <a href="index.php?spect=<?=$tabSpectaclesOBJ[1]->getIdSpectacle()?>">
                            <img src="<?=$tabSpectaclesOBJ[1]->getImg()?>" style="height: 600px;width: 424px" class="C-slide" alt="">
                            <div class="carousel-caption">
                                <h3><?=$tabSpectaclesOBJ[1]->getTitre()?></h3>
                                <p><?=$tabSpectaclesOBJ[1]->getAccroche()?></p>
                            </div>
                        </a>
                    </div>

                    <div class="item" style="height: 50%">
                        <a href="index.php?spect=<?=$tabSpectaclesOBJ[2]->getIdSpectacle()?>">
                            <img src="<?=$tabSpectaclesOBJ[2]->getImg()?>" style="height: 600px;width: 424px" class="C-slide" alt="">
                            <div class="carousel-caption">
                                <h3><?=$tabSpectaclesOBJ[2]->getTitre()?></h3>
                                <p><?=$tabSpectaclesOBJ[2]->getAccroche()?></p>
                            </div>
                        </a>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <table class="table table-hover">
            <?php foreach ($tabSpectaclesOBJ as $spectacle) : ?>
                <tr style="background-color: #ebebec">
                    <td class="illustration">
                        <img src="<?=$spectacle->getImg()?>" style="height: 300px;width: 250px" alt="">
                    </td>
                    <td class="description">
                        <h2><?=$spectacle->getTitre()?></h2>
                        <h3><?=$spectacle->getAccroche()?></h3>
                        <p><?=tronquer_texte($spectacle->getDescription())?></p>
                        <p> <b>Durée :</b> <?=$spectacle->getDuree()?> min</p>
                        <a href="index.php?spect=<?=$spectacle->getIdSpectacle()?>" class="list-group-item">Voir details</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </section>
</main>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>
