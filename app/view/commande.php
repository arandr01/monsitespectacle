<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<!-- DEBUT de la page -->
<?php
$controllerHeader->getHeader();
?>
<header>
    <h2>Enregistrement</h2>
</header>
<section>
    <div style="background-color: #cccccc">
        <h3>Récapitulatif de la commande</h3>
        <table class="table-prix">
            <?php foreach ($tabLignes as $ligne) : ?>
                <tbody>
                <tr>
                    <td>
                        <h3><?=$ligne->getNbPlace()?>x </h3>
                    </td>
                    <td>
                        <?=$ligne->getSpectacle()->getTitre()?>
                    </td>
                    <td>
                        <?=$ligne->getNbPlace() * $ligne->getSpectacle()->getPrix()?> €
                    </td>
                </tr>
                </tbody>
            <?php endforeach; ?>
            <td>
                Totaux : <?=$total?> €
            </td>
        </table>
    </div>

</section>
<section>
    <button><a href="index.php?action=payer" >Payer</a></button>
    <form action="index.php?action=commande" method="post">
        <input type="hidden" name="action" value="cancel">
        <input type="submit" name="send" value="annuler">
    </form>
</section>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>