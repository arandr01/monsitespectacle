<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <title>Document</title>
</head>
<body>
<?php
$controllerHeader->getHeader();
?>
<main>
    <div class="container" style="background-color: #ebebec">
        <section>
            <form action="index.php?action=save" method="post">
                <fieldset>
                    <legend><h3>Creation de compte</h3></legend>
                    <p>
                        <label for="Civilite">Civilite : </label>
                        <select name="civilite" id="Civilite">
                            <option value="Mr.">Mr.</option>
                            <option value="Mme.">Mme.</option>
                        </select>
                    </p>
                    <p>
                        <label for="Nom">Nom : </label>
                        <input type="text" id="Nom" name="nom" value="<?=$nom?>" required>
                    </p>
                    <p>
                        <label for="Prenom"> Prenom : </label>
                        <input type="text" id="Prenom" name="prenom" value="<?=$prenom?>" required>
                    </p>
                    <p>
                        <label for="Pays"> Pays : </label>
                        <input type="text" id="Pays" name="pays" value="<?=$pays?>">
                    </p>
                    <p>
                        <label for="Codepostal"> Codepostal : </label>
                        <input type="number" id="Codepostal" name="codepostal" value="<?=$codepostal?>">
                    </p>
                    <p>
                        <label for="Ville"> Ville : </label>
                        <input type="text" id="Ville" name="ville" value="<?=$ville?>" required>
                    </p>
                    <p>
                        <label for="Email"> email : </label>
                        <input type="email" id="Email" name="email" value="<?=$email?>" required>
                    </p>
                    <p>
                        <label for="Mdp"> Mot De Passe : </label>
                        <input type="password" id="Mdp" name="mdp" required>
                    </p>
                    <p>
                        <label for="Tel"> Téléphone : </label>
                        <input type="number" id="Tel" name="tel" value="<?=$tel?>" required>
                    </p>
                </fieldset>
                <input type="submit" id="btnSubmit" name="send" value="Envoyer">
            </form>
        </section>
    </div>
</main>
</body>
</html>
