<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<!-- DEBUT de la page -->
<?php
$controllerHeader->getHeader();
?>
<header>
    <nav>
        <ul>
            <li><a href="index.php?mode=admin"><h2>Ajout de Spectacle</h2></a></li>
            <li><a href="index.php?mode=admin&spect=all"><h2>Spectacles</h2></a></li>
            <li><a href="index.php?mode=admin&transactions=all"><h2>Transactions</h2></a></li>
            <li><a href="index.php?mode=admin&categorie=all"><h2>Categories</h2></a></li>
        </ul>
    </nav>
</header>
<section>
    <div style="background-color: #cccccc" class="container">
        <h3>Récapitulatif des Réservations</h3>
        <?php foreach ($tabReservations as $reservation) : ?>
        <ul class="list-group" style="display: flex">
            <li class="list-group-item">idReservation : <?=$reservation->getIdReservation()?></li>
            <li class="list-group-item">NomClient : <?=$reservation->getClientOBJ()->getNom()?></li>
            <li class="list-group-item">
                <p>Commandes : </p>
                <table class="table-prix">
                    <thead>
                    <tr>
                        <td>
                            <p>Titre</p>
                        </td>
                        <td>
                            <p>NbPlaces</p>
                        </td>
                        <td>
                            <p>Total</p>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($reservation->getLignesReservation() as $ligne) : ?>
                        <tr>
                            <td>
                                <?=$ligne->getSpectacle()->getTitre()?>
                            </td>
                            <td>
                                x<?=$ligne->getNbPlace()?>
                            </td>
                            <td>
                                <?=$ligne->getNbPlace() * $ligne->getSpectacle()->getPrix()?> €
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </li>
            <li class="list-group-item">Total : <?=$reservation->getPrixTotal()?> € </li>
        </ul>
        <?php endforeach; ?>
    </div>
</section>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>