<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script src="assets/js/ajoutArtiste.js"></script>
    <title>Document</title>
</head>
<body>
<?php
$controllerHeader->getHeader();
?>
<main>
<div class="container">
    <h1> Administration du site</h1>
    <header>
        <nav>
            <ul>
                <li><a href="index.php?mode=admin"><h2>Ajout de Spectacle</h2></a></li>
                <li><a href="index.php?mode=admin&spect=all"><h2>Spectacles</h2></a></li>
                <li><a href="index.php?mode=admin&transactions=all"><h2>Transactions</h2></a></li>
                <li><a href="index.php?mode=admin&categorie=all"><h2>Categories</h2></a></li>
            </ul>
        </nav>
    </header>
    <section>
        <form action="index.php?mode=admin&spect=<?=$spectacle->getIdSpectacle()?>" method="post" enctype="multipart/form-data">
            <table class="table table-hover">
                <tr style="background-color: #ebebec">
                    <td>
                        <div>
                            <input type="submit" id="btnSubmit" name="send" value="Envoyer">
                            <h3><?=$status?></h3>
                        </div>
                        <p style="display: block">
                            <label for="image">Image courante : </label>
                            <img src="<?=$spectacle->getImg()?>" style="object-fit: fill;height: 600px;width: 424px" alt="">
                            <input type="file" id="image" name="image" accept="image/jpg" value="<?=$spectacle->getImg()?>" required>
                        </p>
                    </td>
                    <td style="display: flex;flex-wrap: wrap">
                        <p>
                            <label for="Titre">Titre : </label> <br>
                            <textarea name="titre" id="Titre" cols="50" rows="2" required><?=$spectacle->getTitre()?></textarea>
                        </p>
                        <p>
                            <label for="Accroche"> Accroche : </label>
                            <textarea name="accroche" id="Accroche" cols="50" rows="5" required><?=$spectacle->getAccroche()?></textarea>
                        </p>
                        <p>
                            <label for="Description"> Description : </label>
                            <textarea name="description" id="Description" cols="50" rows="20" required><?=$spectacle->getDescription()?></textarea>
                        </p>
                    </td>
                    <td>
                        <p>
                            <label for="Thematique">Thematique : </label>
                            <input type="text" id="Thematique" name="thematique" value="<?=$spectacle->getThematique()?>" required>
                        </p>
                        <p>
                            <label for="Duree"> Durée : </label>
                            <input type="number" id="Duree" name="duree" value="<?=$spectacle->getDuree()?>" required>
                        </p>
                        <p>
                            <label for="Prix"> Durée : </label>
                            <input type="number" id="Prix" name="prix" value="<?=$spectacle->getPrix()?>" required>
                        </p>
                        <p>
                            <label for="SalleNom"> Nom de Salle : </label>
                            <input list="SallesNom" id="SalleNom" name="nomSalle" value="<?=$spectacle->getSalleOBJ()->getNomSalle()?>" required>
                            <label for="SalleLieu"> Lieu : </label>
                            <input list="SallesLieu" id="SalleLieu" name="lieu" value="<?=$spectacle->getSalleOBJ()->getLieu()?>" required>
                            <label for="SalleNbPlaces"> NbPlaces : </label>
                            <input type="number" id="SalleNbPlaces" name="nbPlaces" value="<?=$spectacle->getSalleOBJ()->getNbPlaces()?>" required>
                            <datalist id="SallesNom">
                                <?php foreach ($tabSalle as $salle) : ?>
                                    <option value="<?=$salle->getNomSalle()?>"><?=$salle->getNomSalle()?></option>
                                <?php endforeach;?>
                            </datalist>
                            <datalist id="SallesLieu">
                                <?php foreach ($tabSalle as $salle) : ?>
                                    <option value="<?=$salle->getLieu()?>"><?=$salle->getLieu()?></option>
                                <?php endforeach;?>
                            </datalist>
                        </p>
                        <p>
                            <label for="Categorie">Catégorie : </label>
                            <input list="Categories" id="Categorie" name="categorie" value="<?=$spectacle->getCategorieOBJ()->getType()?>" required>
                            <datalist id="Categories">
                                <?php foreach ($tabCategorie as $categorie) : ?>
                                    <option value="<?=$categorie->getType()?>"><?=$categorie->getType()?></option>
                                <?php endforeach;?>
                            </datalist>
                        </p>
                        <p id="Artiste">
                            <label for="Artiste">Artistes : </label> <br>
                            <?php foreach ($tabArtistesSpectacle as $artisteSpec) : ?>
                                <label for="Artiste-nom" class="a-<?=$artisteSpec->getIdArtiste()?>" > Nom : </label>
                                <input list="Artistes-nom"  name="nom[]" class="a-<?=$artisteSpec->getIdArtiste()?>" value="<?=$artisteSpec->getNom()?>" required>
                                <label for="Artiste-prenom" class="a-<?=$artisteSpec->getIdArtiste()?>"> Prenom : </label>
                                <input list="Artistes-prenom" name="prenom[]" class="a-<?=$artisteSpec->getIdArtiste()?>" value="<?=$artisteSpec->getPrenom()?>">
                                <button type="button" name="suppr" id="<?=$artisteSpec->getIdArtiste()?>">X</button>
                            <?php endforeach; ?>
                            <input type="hidden" id="idArtiste" value="<?=$lastIdArtiste?>"><!----Variable javascript --->
                            <datalist id="Artistes-nom">
                                <?php foreach ($tabArtistes as $artiste) : ?>
                                    <option value="<?=$artiste->getNom()?>"><?=$artiste->getNom()?></option>
                                <?php endforeach;?>
                            </datalist>
                            <datalist id="Artistes-prenom">
                                <?php foreach ($tabArtistes as $artiste) : ?>
                                    <option value="<?=$artiste->getPrenom()?>"><?=$artiste->getPrenom()?></option>
                                <?php endforeach;?>
                            </datalist>
                            <br>
                        </p>
                        <p>
                            <button type="button" id="ajoutArtiste">Ajouter un artiste</button>
                        </p>
                    </td>
                </tr>
            </table>
        </form>
    </section>
</div>
<main/>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>