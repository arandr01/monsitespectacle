<header>
    <nav class="navbar navbar-inverse" >
        <div class="container-fluid" >
            <div class="navbar-header">
                <a class="navbar-brand active" href="#">Mon Site Web</a>
            </div>
            <ul class="nav navbar-nav">
                <li class=""><a href="index.php">Acceuil</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Categorie <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="index.php?cat=all">toutes les catégories</a></li>
                        <?php foreach ($tabCategoriesOBJ as $categories) : ?>
                            <li><a href="index.php?cat=<?=$categories->getIdCategorie()?>"><?=$categories->getType()?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <!----<li class="nav-item"><a href="#">Reservation</a></li>----->
                <form class="navbar-form navbar-left" action="index.php" method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Rechercher..." name="barrecherche">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </ul>
            <ul class="nav navbar-nav navbar-header navbar-right">
                <?php if (!empty($_SESSION['account'])) : ?>
                <li>
                    <a class="navbar-brand" href="#">Bonjour <?=$_SESSION['account']->getCivilite()." ".$_SESSION['account']->getNom()." ".$_SESSION['account']->getPrenom()?> </a>
                </li>
                <?php endif; ?>
                <li class="dropdown">
                    <a class="navbar-brand dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> Profile <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php if (!empty($_SESSION['account'])) : ?>
                            <li><a href="index.php?action=reserver">mes réservations<?=$reservation?></a></li>
                            <li><a href="index.php?action=logout">se déconnecter</a></li>
                        <?php else: ?>
                            <li><a href="index.php?action=login"><span class="glyphicon glyphicon-log-in"></span> Se connecter</a></li>
                            <li><a href="index.php?action=save"><span class="glyphicon glyphicon-log-in"></span> S'inscrire</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
