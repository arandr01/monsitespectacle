<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <title>Document</title>
</head>
<body>
<?php
$controllerHeader->getHeader();
?>
<main>
    <div class="container loginBox">
        <section>
            <div id="formContent">
                <form action="index.php?action=login" method="post">
                    <fieldset>
                        <p>
                            <label for="Email"> Email : </label> <br>
                            <input type="email" id="Email" name="email" value="<?=$email?>" required>
                        </p>
                        <p>
                            <label for="Mdp"> Mot De Passe : </label>
                            <input type="password" id="Mdp" name="mdp" required>
                        </p>
                    </fieldset>
                    <input type="submit" id="btnSubmit" name="send" value="Envoyer">
                </form>
            </div>
        </section>
    </div>
</main>
</body>
</html>
