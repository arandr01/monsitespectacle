<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<?php
$controllerHeader->getHeader();
?>
<main>
    <section class="container" style="display: flex;flex-wrap: wrap;background-color: #ebebec">
        <table class="table table-hover">
            <tr style="background-color: #cccccc">
                <td class="illustration" style="text-align: center;display: flex;-webkit-box-shadow: 0 20px 20px 0 rgba(0,0,0,0.3);width: 250px">
                    <img src="<?=$spectacle->getImg()?>" style="object-fit: fill;height: 300px;width: 225px" alt="">
                </td>
                <td style="width: 250px;display: flex;flex-direction: column;border-right-style: dashed;border-right-width: 10px;border-right-color: black;border-left-style: dashed;border-left-width: 10px;border-left-color: black">
                    <p> <b>Thématique :</b> <?=$spectacle->getThematique()?></p> <br>
                    <p> <b>Durée :</b> <?=$spectacle->getDuree()?> min</p> <br>
                    <p> <b>Catégorie :</b> <?=$spectacle->getCategorieOBJ()->getType()?></p> <br>
                    <p>
                        <b>Lieu :</b> <br><?=$spectacle->getSalleOBJ()->getNomSalle()?> <br>
                        <?=$spectacle->getSalleOBJ()->getLieu()?> <br>
                        ( <?=$spectacle->getSalleOBJ()->getNbPlaces()?> places)
                    </p> <br>
                    <?php foreach ($spectacle->getArtistes() as $artiste ) : ?>
                        <p> <b>Artistes :</b> <br><?=$artiste->getPrenom()?> <?=$artiste->getNom()?></p> <br>
                    <?php endforeach; ?>
                </td>
                <td class="description" style="width: auto;border-radius: 10% / 50%;background-color: burlywood">
                    <h2 style="margin: 40px"><?=$spectacle->getTitre()?></h2>
                    <h3 style="margin: 40px"><?=$spectacle->getAccroche()?></h3>
                    <form action="index.php?action=reserver&spect=<?=$spectacle->getIdSpectacle()?>" method="post">
                        <table style="margin: 40px" class="table-prix">
                            <thead>
                                <tr >
                                    <td>Prix par personnes </td>
                                    <td>Places </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?=$spectacle->getPrix()?> € </td>
                                    <td>
                                        <label for="Place"> Nombres de places : </label>
                                        <input type="number" id="Place" name="billets" min="0">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="margin: 40px">
                            <input type="hidden" name="spect" value="<?=$spectacle->getIdSpectacle()?>">
                            <input type="submit" name="send" value="Réserver">
                            <button><a href="index.php?spect=<?=$spectacle->getIdSpectacle()?>">Retour</a></button>
                        </div>
                    </form>
                </td>
            </tr>
        </table>
    </section>
</main>
<?php
$controllerFooter->getFooter();
?>
</body>
</html>
