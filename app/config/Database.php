<?php
namespace App\config;
class Database
{
    private string $host;
    private string $db_name;
    private string $username;
    private string $password;
    private object $conn;

    /**
     * @param string $host
     * @param string $db_name
     * @param string $username
     * @param string $password
     * @param object $conn
     */
    public function __construct()
    {
        $this->host = "localhost";
        $this->db_name = "sitespectacle";
        $this->username = "root";
        $this->password = "";
    }


    public function getConnection(){
        try{
            $this->conn = new \PDO("mysql:host=".$this->host.";dbname=".$this->db_name,$this->username,$this->password);
            $this->conn->exec("set names utf8");
        }
        catch (PDOException $exception){
            echo "Connection error: ".$exception->getMessage();
        }
        return $this->conn;
    }
}