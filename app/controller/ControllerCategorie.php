<?php

namespace app\controller;

use app\model\ModelSpectacle;

class ControllerCategorie
{
    private $model;
    private $cat;

    public function __construct()
    {
        $ModelSpectacle = new ModelSpectacle();
        $this->model = $ModelSpectacle;
        if(isset($_GET['cat'])){
            $this->cat = $_GET['cat'];
        }
    }


    public function getAll()
    {
        setcookie("categorie",$_GET['cat'],time()+24*60*60);
        $tabSpectaclesOBJ = array();
        if($_GET['cat'] == 'all'){
            $tabSpectaclesOBJ = $this->model->findAll();
        }
        else{
            $tabSpectaclesOBJ = $this->model->findParCategorie($this->getCat());
        }
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ('app/view/getByCategorie.php');
    }
    public function getAllSearchBar(string $recherche)
    {
        $tabSpectaclesOBJ = $this->model->findByTitle($recherche);
        if(!empty($tabSpectaclesOBJ)){
            setcookie("categorie",$tabSpectaclesOBJ[0]->getCategorie(),time()+24*60*60);
        }
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ('app/view/getByCategorie.php');
    }
    /**
     * @return int
     */
    public function getCat(): int
    {
        return $this->cat;
    }

    /**
     * @param int $cat
     */
    public function setCat(int $cat): void
    {
        $this->cat = $cat;
    }

}