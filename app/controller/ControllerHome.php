<?php

namespace app\controller;

use app\model\ModelSpectacle;
class ControllerHome
{
    private $model;

    /**
     * @param $model
     */
    public function __construct()
    {
        $ModelSpectacle = new ModelSpectacle();
        $this->model = $ModelSpectacle;
    }

    public function getSpectacles(){
        if (isset($_COOKIE['categorie']) && !empty($_COOKIE['categorie'])) {
            if ($_COOKIE['categorie'] != 'all') {
                $categorie = $_COOKIE['categorie'];
                $tabSpectaclesOBJ = $this->model->find3ParCategorie($categorie);
                if(count($tabSpectaclesOBJ) < 3){
                    $limit = 3 - count($tabSpectaclesOBJ);
                    $randomSpectacles = $this->model->findRandom($limit,$tabSpectaclesOBJ);
                    $tabSpectaclesOBJ = array_merge($tabSpectaclesOBJ,$randomSpectacles);
                }
            }
            else{
                $tabSpectaclesOBJ = $this->model->find3();
            }
        }
        else{
            $tabSpectaclesOBJ = $this->model->find3();
        }
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ("app/view/getAllSpectacles.php");
    }
}