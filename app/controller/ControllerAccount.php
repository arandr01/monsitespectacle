<?php

namespace app\controller;

use app\model\ModelClient;

class ControllerAccount
{
    private $model;

    public function __construct()
    {
        $ModelClient = new ModelClient();
        $this->model = $ModelClient;
    }

    public function signUp()
    {
        $this->model->createAccount();
        if(!empty($_POST)){
            $_SESSION['civilite'] = addslashes($_POST['civilite']);
            $civilite = $_SESSION['civilite'];
            $_SESSION['nom'] = addslashes($_POST['nom']);
            $nom = $_SESSION['nom'];
            $_SESSION['prenom'] = addslashes($_POST['prenom']);
            $prenom = $_SESSION['prenom'];
            $_SESSION['pays'] = addslashes($_POST['pays']);
            $pays = $_SESSION['pays'];
            $_SESSION['codepostal'] = addslashes($_POST['codepostal']);
            $codepostal = $_SESSION['codepostal'];
            $_SESSION['ville'] = addslashes($_POST['ville']);
            $ville = $_SESSION['ville'];
            $_SESSION['email'] = addslashes($_POST['email']);
            $email = $_SESSION['email'];
            $_SESSION['tel'] = addslashes($_POST['tel']);
            $tel = $_SESSION['tel'];
        }
        else{
            $civilite = "";
            $nom = "";
            $prenom = "";
            $pays = "";
            $codepostal = "";
            $ville = "";
            $email = "";
            $tel = "";
        }
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ('app/view/signUp.php');
    }

    public function login()
    {
        if(isset($_POST['email'],$_POST['mdp'])) {
            if (!empty($_POST['email']) && !empty($_POST['mdp'])) {
                $email = addslashes($_POST['email']);
                $mdp = addslashes($_POST['mdp']);
                $account = array(
                    'email'=>$email,
                    'mdp'=>$mdp
                );
                $this->model->login($account);
            }
            else {
                print("Remplissez tous les champs");
            }
        }
        if(!empty($_POST)){
            $_SESSION['email'] = addslashes($_POST['email']);
            $email = $_SESSION['email'];
        }
        else{
            $email = "";
        }
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ('app/view/login.php');
    }
    public function logout()
    {
        require ('app/view/logout.php');
    }
}