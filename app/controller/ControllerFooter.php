<?php

namespace app\controller;

use app\model\ModelAdmin;

class ControllerFooter
{
    private ModelAdmin $modelAdmin;
    public function __construct()
    {
        $this->modelAdmin = new ModelAdmin();
    }
    public function getFooter()
    {
        if(isset($_SESSION['account'])){
            $Admin = $this->modelAdmin->findAdmin($_SESSION['account']->getEmail());
        }
        require ("app/view/footer.php");
    }
}