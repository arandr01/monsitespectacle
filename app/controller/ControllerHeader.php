<?php

namespace app\controller;

use app\model\ModelCategorie;

class ControllerHeader
{
    private $model;
    private string $reservation = '';
    /**
     * @param $model
     */
    public function __construct()
    {
        $ModelCategorie = new ModelCategorie();
        $this->model = $ModelCategorie;
        if (isset($_SESSION['reservations'])) {
            $this->reservation = "(" . count(array_keys($_SESSION['reservations'])) . ")";
        }
    }

    /**
     * @return string
     */
    public function getReservation(): string
    {
        return $this->reservation;
    }

    /**
     * @param string $reservation
     */
    public function setReservation(string $reservation): void
    {
        $this->reservation = $reservation;
    }

    public function getHeader()
    {
        $reservation = $this->getReservation();
        $tabCategoriesOBJ = $this->model->findAll();
        require ("app/view/header.php");
    }
}