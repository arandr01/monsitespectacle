<?php

namespace app\controller;

use app\model\ModelLigneReservation;
use app\model\ModelReservation;
use app\controller\ControllerFooter;
use app\controller\ControllerHeader;

class ControllerReservation
{
    private $modelReservation;
    private $modelLigneReservation;
    private int $total;

    public function __construct()
    {
        $ModelReservation = new ModelReservation();
        $ModelLigneReservation = new ModelLigneReservation();
        $this->total = 0;
        $this->modelReservation = $ModelReservation;
        $this->modelLigneReservation = $ModelLigneReservation;
    }
    public function getReservations()
    {
        $total = $this->getTotal();
        if(isset($_SESSION['idReservation'])){
            header('Location: index.php?action=commande');
        }
        elseif(!empty($_POST['supprime'])){
            unset($_SESSION['reservations'][$_POST['supprime']]);
        }
        $controllerHeader = new ControllerHeader();
        $controllerFooter = new ControllerFooter();
        require ('app/view/mesReservations.php');
    }

    public function commander()
    {
        if(isset($_POST['action']) && $_POST['action'] == 'cancel'){
            $this->modelLigneReservation->deleteLignesReservation($_SESSION['idReservation']);
            $this->modelReservation->deleteReservation($_SESSION['idReservation']);
            unset($_SESSION['idReservation']);
            unset($_SESSION['prixTot']);
            header('Location: index.php?action=reserver');
        }
        if(empty($_SESSION['account'])){
            if(empty($_SESSION['reservations'])){
                header('Location: index.php');
            }
            else{
                header('Location: index.php?action=login');
            }
        }

        elseif (isset($_SESSION['idReservation'])){
            $tabLignes = $this->modelLigneReservation->findAll($_SESSION['idReservation']);
            if(isset($_SESSION['prixTot'])){
                $total = $_SESSION['prixTot'];
            }
            else{
                $total = $_POST['prixTot'];
            }
        }
        else{
            $reservation = $this->modelReservation->startCommande($_POST['prixTot']);
            $total = $reservation->getPrixTotal();
            $_SESSION['prixTot'] = $total;
            $_SESSION['idReservation'] = $reservation->getIdReservation();
            foreach ($_SESSION['reservations'] as $ligne){
                $data = array(
                    'fields'=>'idReservation,idSpectacle,nbPlace',
                    'values'=> $reservation->getIdReservation().",".$ligne['spect']->getIdSpectacle().",".$ligne['billets']
                );
                $this->modelLigneReservation->saveLines($data);
                $tabLignes = $this->modelLigneReservation->findAll($reservation->getIdReservation());
            }
        }
        $controllerHeader = new ControllerHeader();
        $controllerFooter = new ControllerFooter();
        require ('app/view/commande.php');
    }

    public function getPayment()
    {
        unset($_SESSION['reservations']);
        unset($_SESSION['idReservation']);
        unset($_SESSION['prixTot']);
        $controllerHeader = new ControllerHeader();
        $controllerFooter = new ControllerFooter();
        require ('app/view/paiement.php');
    }
    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

}