<?php

namespace app\controller;

use app\model\ModelSpectacle;

class ControllerVueSpectacle
{
    private $model;
    private int $spect;

    public function __construct()
    {
        $ModelSpectacle = new ModelSpectacle();
        $this->model = $ModelSpectacle;
        $this->spect = $_GET['spect'];
    }

    /**
     * @return int|mixed
     */
    public function getSpect(): mixed
    {
        return $this->spect;
    }

    /**
     * @param int|mixed $spect
     */
    public function setSpect(mixed $spect): void
    {
        $this->spect = $spect;
    }


    public function getOne()
    {
        $spectacle = $this->model->findOne($this->getSpect());
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();

        setcookie("categorie",$spectacle->getCategorie(),time()+24*60*60);
        require ("app/view/getOneSpectacle.php");
    }

    public function getVueReservation()
    {
        $spectacle = $this->model->findOne($this->getSpect());
        setcookie("categorie",$spectacle->getCategorie(),time()+24*60*60);
        if (isset($_POST['spect'])){
            if(isset($_SESSION['account'])){
                if ($_POST['billets'] > 0){
                    $_SESSION['reservations'][$_POST['spect']] = array(
                        'spect' => $spectacle,
                        'billets' => $_POST['billets'],
                    );
                }
            }
            else{
                header('Location: index.php?action=login');
            }
        }
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ("app/view/vueReservation.php");
    }

}