<?php

namespace app\controller;

use app\entity\Spectacle;
use app\model\ModelArtiste;
use app\model\ModelCategorie;
use app\model\ModelReservation;
use app\model\ModelSalle;
use app\model\ModelSpectacle;

class ControllerAdmin
{
    private ModelSalle $modelSalle;
    private ModelArtiste $modelArtiste;
    private ModelCategorie $modelCategorie;
    private ModelSpectacle $modelSpectacle;
    private ModelReservation $modelReservation;

    public function __construct()
    {
        $modelSalle = new ModelSalle();
        $modelArtiste = new ModelArtiste();
        $modelCategorie = new ModelCategorie();
        $modelSpectacle = new ModelSpectacle();
        $this->modelSalle = $modelSalle;
        $this->modelArtiste = $modelArtiste;
        $this->modelCategorie = $modelCategorie;
        $this->modelSpectacle = $modelSpectacle;
        $this->modelReservation = new ModelReservation();
    }

    public function createForm()
    {
        if(isset($_POST['titre'],$_POST['thematique'],$_POST['duree'],$_POST['accroche'],$_POST['description'],$_POST['categorie'],$_POST['nomSalle'],$_POST['lieu'],$_POST['nbPlaces'],$_POST['nom'],$_FILES['image'],$_POST['prix'],$_POST['nom'])){
            if(!empty($_POST['titre'] && !empty($_POST['thematique']) && !empty($_POST['duree']) && !empty($_POST['accroche']) && !empty($_POST['description']) && !empty($_POST['categorie']) && !empty($_POST['nomSalle']) && !empty($_POST['lieu']) && !empty($_POST['nbPlaces']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_FILES['image']) && !empty($_POST['prix']))){
                $categorie = addslashes($_POST['categorie']);
                $titre = addslashes($_POST['titre']);
                $thematique = addslashes($_POST['thematique']);
                $duree = addslashes($_POST['duree']);
                $accroche = addslashes($_POST['accroche']);
                $description = addslashes($_POST['description']);
                $nomSalle = addslashes($_POST['nomSalle']);
                $lieu = addslashes($_POST['lieu']);
                $nbPlaces = addslashes($_POST['nbPlaces']);
                $prix = addslashes($_POST['prix']);
                $img = "assets/images/spectacle/".$_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'],$img);
                //Categorie
                $categorieOBJ = $this->modelCategorie->findByType($categorie);
                if(empty($categorieOBJ)){
                    $idCategorie = $this->modelCategorie->saveCategorie($categorie);
                }
                else{
                    $idCategorie = $categorieOBJ->getIdCategorie();
                }

                //Salle
                $salleOBJ = $this->modelSalle->findByNom($nomSalle);
                if(empty($salleOBJ)){
                    $dataSalle = array(
                        'nomSalle'=> $nomSalle,
                        'lieu' => $lieu,
                        'nbPlaces' => $nbPlaces
                    );
                    $idSalle= $this->modelSalle->saveSalle($dataSalle);
                }
                else{
                    $idSalle = $salleOBJ->getIdSalle();
                }

                //Spectacles
                $dataSpectacle = array(
                    'titre'=> str_replace(',',';',$titre),
                    'thematique'=>str_replace(',',';',$thematique),
                    'duree'=>$duree,
                    'accroche'=>str_replace(',',';',$accroche),
                    'description'=>str_replace(',',';',$description),
                    'categorie'=>$idCategorie,
                    'salle'=>$idSalle,
                    'img' =>$img,
                    'prix'=>str_replace(',',';',$prix)
                );
                $spectacle = new Spectacle($dataSpectacle);
                $idSpectacle = $this->modelSpectacle->saveSpectacle($spectacle);

                //Artistes
                $this->modelArtiste->saveArtistes(array_combine($_POST['nom'],$_POST['prenom']),$idSpectacle);

            }
            else{
                print("Remplissez tout les champs");
            }
        }
        $tabArtistes = $this->modelArtiste->findAll();
        $tabSalle = $this->modelSalle->findAll();
        $tabCategorie = $this->modelCategorie->findAll();
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ('app/view/insertSpectacle.php');
    }

    public function getAll()
    {
        if(isset($_POST['supprimer']) && !empty($_POST['supprimer'])){
            foreach ($_POST['supprimer'] as $idspectacle){
                $this->modelArtiste->deletesArtistes($idspectacle);
                $this->modelSpectacle->deleteSpectacle($idspectacle);
            }
        }
        $tabSpectaclesOBJ = array();
        $tabSpectaclesOBJ = $this->modelSpectacle->findAll();
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ('app/view/manageSpectacle.php');
    }

    public function getOne(int $idSpectacle)
    {
        $spectacle = $this->modelSpectacle->findOne($idSpectacle);
        $status = '';
        //var_dump($_POST);
        if(isset($_POST['titre'],$_POST['thematique'],$_POST['duree'],$_POST['accroche'],$_POST['description'],$_POST['categorie'],$_POST['nomSalle'],$_POST['lieu'],$_POST['nbPlaces'],$_POST['nom'],$_POST['prenom'],$_POST['nom'],$_FILES['image'], $_POST['prix'])) {
            if (!empty($_POST['titre'] && !empty($_POST['thematique']) && !empty($_POST['duree']) && !empty($_POST['accroche']) && !empty($_POST['description']) && !empty($_POST['categorie']) && !empty($_POST['nomSalle']) && !empty($_POST['lieu']) && !empty($_POST['nbPlaces']) && !empty($_POST['nom']) && !empty($_POST['nom']) && !empty($_FILES['image']) && !empty($_POST['prix']))) {
                $categorie = addslashes($_POST['categorie']);
                $titre = addslashes($_POST['titre']);
                $thematique = addslashes($_POST['thematique']);
                $duree = addslashes($_POST['duree']);
                $accroche = addslashes($_POST['accroche']);
                $description = addslashes($_POST['description']);
                $nomSalle = addslashes($_POST['nomSalle']);
                $lieu = addslashes($_POST['lieu']);
                $nbPlaces = addslashes($_POST['nbPlaces']);
                $prix = addslashes($_POST['prix']);
                $img = "assets/images/spectacle/".$_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'],$img);
                //Categorie
                $categorieOBJ = $this->modelCategorie->findByType($categorie);
                if(empty($categorieOBJ)){
                    $idCategorie = $this->modelCategorie->saveCategorie($categorie);
                }
                else{
                    $idCategorie = $categorieOBJ->getIdCategorie();
                }
                //Salle
                $salleOBJ = $this->modelSalle->findByNom($nomSalle);
                if(empty($salleOBJ)){
                    $dataSalle = array(
                        'nomSalle'=> $nomSalle,
                        'lieu' => $lieu,
                        'nbPlaces' => $nbPlaces
                    );
                    $idSalle= $this->modelSalle->saveSalle($dataSalle);
                }
                else{
                    $idSalle = $salleOBJ->getIdSalle();
                }
                //Artistes
                $this->modelArtiste->deletesArtistes($spectacle->getIdSpectacle());
                $this->modelArtiste->saveArtistes(array_combine($_POST['nom'],$_POST['prenom']),$spectacle->getIdSpectacle());
                //Spectacles
                $dataSpectacle = array(
                    'titre'=> str_replace(',',';',$titre),
                    'thematique'=>str_replace(',',';',$thematique),
                    'duree'=>$duree,
                    'accroche'=>str_replace(',',';',$accroche),
                    'description'=>str_replace(',',';',$description),
                    'categorie'=>$idCategorie,
                    'salle'=>$idSalle,
                    'img' =>$img,
                    'prix'=>$prix,
                );
                $spectacle->updates($dataSpectacle);
                $this->modelSpectacle->updateSpectacle($spectacle);
                $status = "Table Mise a jour";
            }
            else{
                print("Remplissez tout les champs");
            }
        }
        $tabArtistes = $this->modelArtiste->findAll();
        $lastIdArtiste = $tabArtistes[count($tabArtistes)-1]->getIdArtiste();
        $tabArtistesSpectacle = $this->modelArtiste->findAllSpectacleArtiste($spectacle->getIdSpectacle());
        $tabSalle = $this->modelSalle->findAll();
        $tabCategorie = $this->modelCategorie->findAll();
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ("app/view/modification.php");
    }

    function getAllCat()
    {
        if (isset($_POST['send']) || isset($_POST['delete'])){
            if(!empty($_POST['send'])){
                foreach ($_POST['idCategories'] as $idCategorie){
                    $categorie = $this->modelCategorie->findOne($idCategorie);
                    $this->modelCategorie->updateCategorie($categorie);
                    print("Table mise à jour ");
                }
            }
            elseif (!empty($_POST['delete'])){
                foreach ($_POST['supprimer'] as $idCategorie){
                    $this->modelCategorie->deleteCategorie($idCategorie);
                }
            }

        }
        $tabCategorie = $this->modelCategorie->findAll();
        $lastIdCat = $tabCategorie[count($tabCategorie)-1];
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();

        require ("app/view/manageCategorie.php");
    }

    public function getAllTransactions()
    {
        $tabReservations = $this->modelReservation->findAll();
        $controllerHeader = new \app\controller\ControllerHeader();
        $controllerFooter = new \app\controller\ControllerFooter();
        require ("app/view/transactionsLog.php");
    }
}