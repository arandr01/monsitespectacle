<?php
use app\controller\ControllerHome;
use app\controller\ControllerVueSpectacle;
use app\controller\ControllerCategorie;
use app\controller\ControllerAccount;
use app\controller\ControllerReservation;
use app\controller\ControllerAdmin;
function chargerClasse($classe)
{
    $classe=str_replace('\\','/',$classe);
    require $classe.'.php';
}
spl_autoload_register('chargerClasse');

session_start();
function tronquer_texte($texte, $longeur_max = 100)
{
    if (strlen($texte) > $longeur_max)
    {
        $texte = substr($texte, 0, $longeur_max);
        $texte .= "...";
    }
    return $texte;
}
////-------Rooters

//controllers
$controllerHome = new ControllerHome();

///Vue Spectacle
if(isset($_GET['spect']) && !empty($_GET['spect'] && !isset($_GET['mode'])))
{
    $controllerVueSpectacle = new ControllerVueSpectacle();
    if(isset($_GET['action']) && $_GET['action'] == 'reserver')
    {
        $controllerVueSpectacle->getVueReservation();
    }
    else{
        $controllerVueSpectacle->getOne();
    }
}

//Recherche et Categories
elseif (isset($_POST['barrecherche']))
{
    $controllerCategorie = new ControllerCategorie();
    $recherche = htmlspecialchars($_POST['barrecherche']);
    $controllerCategorie->getAllSearchBar($recherche);
}
elseif(isset($_GET['cat']) && !empty($_GET['cat']))
{
    $controllerCategorie = new ControllerCategorie();
    $controllerCategorie->getAll();
}


//Connexion
elseif(isset($_GET['action'])&& $_GET['action'] == 'save')
{
    $controllerAccount = new ControllerAccount();
    $controllerAccount->signUp();
}
elseif(isset($_GET['action'])&& $_GET['action'] == 'login')
{
    $controllerAccount = new ControllerAccount();
    $controllerAccount->login();
}
elseif(isset($_GET['action']) && $_GET['action'] == 'logout')
{
    $controllerAccount = new ControllerAccount();
    $controllerAccount->logout();
}
//
//Administration
elseif(isset($_GET['mode']) && $_GET['mode']=='admin')
{
    $controllerAdmin = new ControllerAdmin();
    if(isset($_GET['spect']) && !empty($_GET['spect']) && $_GET['spect'] != 'all'){
        $controllerAdmin->getOne($_GET['spect']);
    }
    elseif(isset($_GET['spect']) && $_GET['spect'] == 'all'){
        $controllerAdmin->getAll();
    }
    elseif (isset($_GET['transactions'])&& $_GET['transactions'] == 'all'){
        $controllerAdmin->getAllTransactions();
    }
    elseif (isset($_GET['categorie']) && $_GET['categorie'] == 'all'){
        $controllerAdmin->getAllCat();
    }
    else{
        $controllerAdmin->createForm();
    }
}
//RESERVATION
elseif(isset($_GET['action']) && $_GET['action']=='reserver')
{
    $controllerReservation = new ControllerReservation();
    $controllerReservation->getReservations();
}
elseif(isset($_GET['action']) && $_GET['action'] == 'commande')
{
    $controllerReservation = new ControllerReservation();
    $controllerReservation->commander();
}
elseif(isset($_GET['action'])&& $_GET['action'] == 'payer')
{
    $controllerReservation = new ControllerReservation();
    $controllerReservation->getPayment();
}
//
else{
    $controllerHome->getSpectacles();
}
//var_dump($_SESSION['account']->getNom());
